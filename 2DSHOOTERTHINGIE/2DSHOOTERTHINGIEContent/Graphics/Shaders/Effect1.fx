sampler s0; 
float param1;  

float4 PixelShaderFunction(float2 coords: TEXCOORD0) : COLOR0  
{  
    float4 color = tex2D(s0, coords);

	//color.gb = color.r;
	if (coords.y > param1)  
    color = float4(0,0,0,0);  
	   
	return color; 
}  
      
technique Technique1  
{  
        pass Pass1  
        {  
            PixelShader = compile ps_2_0 PixelShaderFunction();  
        }  
}  
