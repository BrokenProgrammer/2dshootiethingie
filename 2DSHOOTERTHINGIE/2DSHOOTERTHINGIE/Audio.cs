﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    class Audio
    {
        SoundEffect DeathSound1;
        SoundEffect DeathSound2;
        SoundEffect DeathSound3;

        SoundEffect DestroyedEquipmentSparkSound1;
        SoundEffect DestroyedEquipmentSparkSound2;
        SoundEffect DestroyedEquipmentSparkSound3;

        SoundEffect WeaponGrabSound1;
        SoundEffect WeaponGrabSound2;

        SoundEffect JumpingSound;
        SoundEffectInstance JumpingSoundInstance;
        SoundEffect LandingSound;
        SoundEffectInstance LandingSoundInstance;
        SoundEffect WalkingSound;
        SoundEffectInstance WalkingSoundInstance;
        SoundEffect RunningSound;
        SoundEffectInstance RunningSoundInstance;

        SoundEffect StabbedInBackSound;
        SoundEffect StabbedInFrontSound;
        SoundEffect KnifeStabSound;

        SoundEffect GunFireSound;
        SoundEffect HitByGunSound;
        SoundEffect BazookaFireSound;
        SoundEffect BazookaExplosionSound;

        SoundEffect ExplosionSound1;
        SoundEffect ExplosionSound2;
        SoundEffect ExplosionSound3;

        Song MenuMusic;
        Song GOTTAGOFAST;

        public void LoadIngameAudio(ContentManager content) //Load in audio for ingame
        {
            //SOUND EFFECTS
            //Note - The end name of the sound effects are initials of the effects themselves
            DeathSound1 = content.Load<SoundEffect>("Audio/Sound Effects/PlayerDeath/DS1");
            DeathSound2 = content.Load<SoundEffect>("Audio/Sound Effects/PlayerDeath/DS2");
            DeathSound3 = content.Load<SoundEffect>("Audio/Sound Effects/PlayerDeath/DS3");

            DestroyedEquipmentSparkSound1 = content.Load<SoundEffect>("Audio/Sound Effects/Equipment/DESS1");
            DestroyedEquipmentSparkSound2 = content.Load<SoundEffect>("Audio/Sound Effects/Equipment/DESS2");
            DestroyedEquipmentSparkSound3 = content.Load<SoundEffect>("Audio/Sound Effects/Equipment/DESS3");

            WeaponGrabSound1 = content.Load<SoundEffect>("Audio/Sound Effects/Weapons/WGS1");
            WeaponGrabSound2 = content.Load<SoundEffect>("Audio/Sound Effects/Weapons/WGS2");

            JumpingSound = content.Load<SoundEffect>("Audio/Sound Effects/PlayerJump/JS");
            LandingSound = content.Load<SoundEffect>("Audio/Sound Effects/PlayerLand/LS");
            WalkingSound = content.Load<SoundEffect>("Audio/Sound Effects/PlayerWalk/WS");
            RunningSound = content.Load<SoundEffect>("Audio/Sound Effects/PlayerRun/RS");

            StabbedInBackSound = content.Load<SoundEffect>("Audio/Sound Effects/Weapons/SIBS");
            StabbedInFrontSound = content.Load<SoundEffect>("Audio/Sound Effects/PlayerDeath/SIFS");
            KnifeStabSound = content.Load<SoundEffect>("Audio/Sound Effects/PlayerDeath/KSS");

            GunFireSound = content.Load<SoundEffect>("Audio/Sound Effects/Weapons/GFS");
            HitByGunSound = content.Load<SoundEffect>("Audio/Sound Effects/Weapons/HBGS");
            BazookaFireSound = content.Load<SoundEffect>("Audio/Sound Effects/Weapons/BFS");
            BazookaExplosionSound = content.Load<SoundEffect>("Audio/Sound Effects/Weapons/BES");

            ExplosionSound1 = content.Load<SoundEffect>("Audio/Sound Effects/Explosions/ES1");
            ExplosionSound2 = content.Load<SoundEffect>("Audio/Sound Effects/Explosions/ES2");
            ExplosionSound3 = content.Load<SoundEffect>("Audio/Sound Effects/Explosions/ES3");

            //SONGS
            GOTTAGOFAST = content.Load<Song>("Audio/Music/SANIC");

            //INSTANCES
            JumpingSoundInstance = JumpingSound.CreateInstance();
            LandingSoundInstance = LandingSound.CreateInstance();
            WalkingSoundInstance = WalkingSound.CreateInstance();
            RunningSoundInstance = RunningSound.CreateInstance();
        }

        public void LoadMainMenuAudio(ContentManager content) //Load in audio for main menu
        {
            MenuMusic = content.Load<Song>("Audio/Music/METUL1");
        }

        public void UnloadIngameAudio() //Unload currently loaded audio for ingame
        {
            GOTTAGOFAST.Dispose();
        }

        public void UnloadMainMenuAudio() //Unload currently loaded audio for main menu
        {
            MenuMusic.Dispose();
        }
    }
}
