﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    public class Profile
    {
        public void AddFriend()
        {
        }

        public void RemoveFriend()
        {
        }

        public string ProfileName;

        public int TotalDeaths;
        public int TotalKills;
        public int KillDeathRatio;

        public int TotalPlayTime;
        public int PlayerType1Time;
        public int PlayerType2Time;
        public int PlayerType3Time;
        public int PlayerType4Time;

        public enum ProfileState
        {

        }

        public void Update(GameTime gameTime)
        {
            
        }
    }
}
