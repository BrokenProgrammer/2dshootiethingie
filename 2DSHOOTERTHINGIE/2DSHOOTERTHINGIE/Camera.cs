﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    public class Camera 
    {
        private Matrix transform; //Create private transform
        public Matrix Transform //create public transform
        {
            get { return transform; }
        }

        private Vector2 centre; //Define the center of the camera
        private Viewport viewport; //Make a private viewport
        public Vector2 currentpos; //Current position of camera
        public Vector2 virtualRes = new Vector2(1600, 900); //WIDESCREEN : 1600X900 - LETTERBOX : 1280X1024 - The virtual resolution is the basis resolution for all other visual resolutions in the game.

        //THIS IS FOR SPLITSCREEN ONLY
        public Viewport TwoPlayerViewPortVertical; //Split half the screen by the screen width
        public Viewport TwoPlayerViewPortHorisontal; //Split half the screen by the screen height
        public Viewport FourPlayerViewPort;

        //THIS IS FOR SAMESCREEN ONLY
        //public Vector2[] Positions = new Vector2[3];

        private float zoom = 1; //Set the zoom of the camera
        private float rotation = 0; //Set the rotation of the camera
        public float resscalex; //The vertical resolution scale value
        public float resscaley; //The horizontal resolution scale value
        public float aspectus;

        public float X //set the X coordinate
        {
            get { return centre.X; }
            set { centre.X = value; }
        }

        public float Y //Dito as previous function, except for the Y coordinate being the difference
        {
            get { return centre.Y; }
            set { centre.Y = value; }
        }

        public float Zoom //Used for zooming
        {
            get { return zoom; }
            set
            {
                zoom = value;
                if (zoom < 0.1f)
                    zoom = 0.1f;
            }
        }

        public float Rotation //Used for setting the rotation of the camera
        {
            get { return rotation; }
            set { rotation = value; }
        }

        public Camera(Viewport newViewport)
        {
            viewport = newViewport; //Set the private viewport's value as the official viewport's value
            resscalex = (float)Settings.DisplayWidth / (float)virtualRes.X;
            resscaley = (float)Settings.DisplayHeight / (float)virtualRes.Y;
            //aspectus = Settings.DisplayWidth / Settings.DisplayHeight;
            //aspectus = Math.Min(resscalex, resscaley);
        }

        public void Update(Vector2 position) //Update the camera. Send in a position for the camera to focus at.
        {
            centre = new Vector2(position.X, position.Y); //Center the camera on the position

            resscalex = (float)Settings.DisplayWidth / (float)virtualRes.X;
            resscaley = (float)Settings.DisplayHeight / (float)virtualRes.Y;
            //aspectus = (float)Settings.DisplayWidth / Settings.DisplayHeight;

            if (Game1.SplitScreenMode == true)
            {
                transform = Matrix.CreateTranslation(new Vector3(-centre.X, -centre.Y, 0))
                * Matrix.CreateRotationZ(Rotation)
                * Matrix.CreateScale(new Vector3((Zoom * resscalex), (Zoom * resscaley), 0))
                * Matrix.CreateTranslation(new Vector3((float)Settings.DisplayWidth / 2f, (float)Settings.DisplayHeight / 2f, 0));
            }

            if (Game1.NetworkMode == true)
            {
                transform = Matrix.CreateTranslation(new Vector3(-centre.X, -centre.Y, 0))
                * Matrix.CreateRotationZ(Rotation)
                * Matrix.CreateScale(new Vector3((Zoom * resscalex), (Zoom * resscaley), 0))
                * Matrix.CreateTranslation(new Vector3((float)Settings.DisplayWidth / 2f, (float)Settings.DisplayHeight / 2f, 0));
            }

            if (Game1.SameScreenMode == true)
            {
                 transform = Matrix.CreateTranslation(new Vector3(-centre.X, -centre.Y, 0))
                * Matrix.CreateRotationZ(Rotation)
                * Matrix.CreateScale(new Vector3((Zoom * resscalex), (Zoom * resscaley), 0))
                * Matrix.CreateTranslation(new Vector3((float)Settings.DisplayWidth / 2f, (float)Settings.DisplayHeight / 2f, 0));
            }
        }

        public void CheckOffSet(Camera camera) //Check the offset of the camera - correct camera positioning if offset is wrong
        {
            if (camera.currentpos == null) //Lol i dunno (.W.)
            {
                //MAKE SURE CAMERA AIN'T OUT OF CLAMPED RANGE!!!
            }
        }

        public void SetCameraClampX(float FirstPosition, float SecondPosition) //Set a horisontal clamp on the camera
        {
            currentpos.X = MathHelper.Clamp(currentpos.X, FirstPosition, SecondPosition);
        }

        public void SetCameraClampY(float FirstPosition, float SecondPosition) //Set a vertical clamp on the camera
        {
            currentpos.Y = MathHelper.Clamp(currentpos.Y, FirstPosition, SecondPosition);
        }

        //public void AcknowledgePlayerPositions(Player[] Players)
        //{
        //    for (int i = 0; i < Players.Length; i++)
        //    {
        //        if (Players.Length <= Positions.Length)
        //        {
        //            Positions[i] = Players[i].Position;
        //        }
        //    }
        //}
    }
}
