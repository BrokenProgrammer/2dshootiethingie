﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    class ParticleSystem
    {
        public Vector2 Position; //The position of the particle system

        Random random; //Make a randomizer
        Texture2D gfx; //The graphics of the particle
        List<Particle> particleList; //Make sure to spawn multiple particles by making a list

        public ParticleSystem(Texture2D texture)
        {
            //Used for defining a new particle system
            gfx = texture;
            particleList = new List<Particle>();
            random = new Random();
            Position = new Vector2(600, 0);
        }

        public Particle GenerateParticle()
        {
            Point maxValue = new Point(100, 100); //Max speed in X and Y
            Point minValue = new Point(-100, -400); //Min speed in X and Y

            Vector2 velocity = new Vector2(random.Next(minValue.X, maxValue.X), random.Next(minValue.Y, maxValue.Y));//Randomize the speed of the spawning particle
            float angle = 0; //Set the start value of the generating particle
            float angularVelocity = (float)random.NextDouble() * 0.1f; //Set a random speed in the angular rotation of the particle

            Color colour = new Color((byte)random.Next(256), (byte)random.Next(256), (byte)random.Next(256)); //Randomize the colour of the particle
            float size = (float)random.NextDouble() * 0.8f; //Randomize the size of the particle

            int ttl = random.Next(300) + 10;

            return new Particle(gfx, Position, velocity, angle, angularVelocity, colour, size, ttl); //Make sure the values of the particle and it's properties are set
        }

        public void Update(GameTime gameTime)
        {
            int total = 5; //Number of particle to add per frame

            for (int i = 0; i < total; i++)
            {
                //Create particle
                particleList.Add(GenerateParticle());
            }

            //Counting backwards from the end
            for (int i = particleList.Count - 1; i >= 0; i--)
            {
                particleList[i].Update(gameTime);

                if (particleList[i].TimeToLive <= 0) //If the particle's lifetime is ending
                    particleList.RemoveAt(i); //Remove the particle
            }
        }

        public void Draw(SpriteBatch spriteBatch) //Draw the particle
        {
            //  spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.NonPremultiplied, null, null, null, null, Game1.Instance.GameCamera.Transform);

            foreach (Particle p in particleList)
            {
                p.Draw(spriteBatch); //Draw each existing particle
            }

            //   spriteBatch.End();
        }
    }
}
