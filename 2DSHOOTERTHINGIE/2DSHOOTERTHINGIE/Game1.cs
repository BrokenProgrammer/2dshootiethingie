using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Nuclex.Input; //NOTE FOR NUCLEX! ANY OTHER INPUTS THAN XBOX 360 CONTROLLERS CAN ONLY BE REACHED BY EXTENDEDPLAYERINDEX!
using Nuclex.Input.Devices; //DITO

namespace _2DSHOOTERTHINGIE
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public GraphicsDeviceManager graphics; //Define graphics manager
        public InputManager input; //Input manager used for alternate gamepads

        SpriteBatch spriteBatch; //Define the spritebatch of the current class

        BufferedKeyboard keyboard; //Buffers keyboard events until Update() is called
        BufferedMouse mouse; //Buffers mouse events until Update() is called
        KeyboardState oldKeyboardState, //Registers what keys the player has pressed before
          currentKeyboardState; //Registers what keys the player is currently pressing
        GamePadState oldPad; //(Only for Xbox 360 controllers)Registers what gamepad buttons the player has pressed before
        ExtendedGamePadState oldPadAlter; //(Only for alternate gamepads/joystick) Registers what gamepad buttons the player has pressed before
        MouseState currentMouseState, oldMouseState; //Registers the movements and the pressed buttons of the player's computer mouse

        System.Windows.Forms.Control control; //Create a control of the form
        System.Windows.Forms.Form form; //Create a window form

        //States
        public enum GameState //These states determines what kind of code will be executed, depending on which state is selected
        {
            MainMenu, //This state has the code for the main menu of the game
            InputSetup, //Setups the input of the game
            Disconnect, //Checks if any player has disconnected from the game
            Loading, //This state is choosen whenever the game is loading
            Ingame, //This state is choosen when the player is in the game
            Settings, //This state is choosen when the player decides to go to the settings screen
            SettingsGamePlay,
            SettingsAudio,
            SettingsGraphics, 
            Lobby, //This state is choosen when the player chooses to play the game
            Pause, //This state is choosen when the player pauses the game
        }

        public GameState state = GameState.Loading; //State is set to Loading, as the game is loading when starting game
        
        //Functions
        /// <summary>
        /// Reset game
        /// </summary>
        public void ResetGame()
        {
            //CURRENTLY UNUSED
        }

        /// <summary>
        /// Play a specific effect
        /// </summary>
        public void PlayEffect()
        {
            //CURRENTLY UNUSED
        }

        /// <summary>
        /// Select credits in main menu
        /// </summary>
        public void SelectCreds()
        {
            MenuSelect = 4;
        }

        /// <summary>
        /// Check for available gamepads
        /// </summary>
        public void CheckAvailable()
        {
            //for (PlayerIndex i = PlayerIndex.One; i <= PlayerIndex.Four; i++)
            //{
            //    GamePadState state = GamePad.GetState(i);
            //    if (state.IsConnected)
            //    {
            //        // TODO: Process state
            //        PlayerActivate[(int)i] = true;
            //    }
            //}

            for (int i = 0; i <= PlayerActivate.Length; i++)
            {
                //GamePadState state = GamePad.GetState(i);
                //DirectInputGamepad.Gamepads
                //if (DirectInputGamepad.Gamepads[i] != null)
                //{
                //    // TODO: Process state
                //    PlayerActivate[i] = true;
                //}
            }
        }

        /// <summary>
        /// Update the input handling - Check if there are any new controllers connected/disconnected
        /// </summary>
        public void UpdateInput()
        {
            DetectDisconnect = false;
            //PlayerIndex index = PlayerIndex.One;
            //for (int i = 0; i < 4; i++, index++)
            //{
            //    if (PlayerActivate[i] &&
            //        !GamePad.GetState(index).IsConnected)
            //    {
            //        DetectDisconnect = true;
            //    }
            //}

            //for (int i = 0; i < PlayerActivate.Length; i++)
            //{
            //    if (PlayerActivate[i] && DirectInputGamepad.Gamepads[i] == null) //If the player is activated but the player's controller isn't...
            //    {
            //        DetectDisconnect = true; //...then detect disconnect and execute desired command.
            //    }
            //}
        }

        #region Integers
        int MenuSelect = 0; //s
        #endregion

        #region Floats
        //public float MouseDistanceToLimit;
        //public float MouseDistanceToLimitX;
        //public float MouseDistanceToLimitY;
        #endregion

        #region Bools
        public bool[] PlayerActivate = new bool[7]; //To check if the players are in the game or not - Also determines the maximum amount of players in the game.
        public bool DetectDisconnect = false; //Detects whether a control is disconnected or not

        public static bool SplitScreenMode = false; //If the game is to be played with a splitscreen
        public static bool SameScreenMode = true; //If the game is to be played with a moving camera that zooms out whenever other player is outside boundries
        public static bool NetworkMode = false; //Is the game is to be played online

        public bool DebugMode = false; //If the debug console should be activated or not
        public bool Loading = false; //Checks whether the game is loading or not
        #endregion

        #region Vectors
        public Vector2 MouseposLocal;
        public Vector2 MouseposWorld;
        public Vector2 MouseDistanceToLimitVector;
        #endregion

        #region Textures
        public Texture2D Enemy1Texture;
        public Texture2D Enemy2Texture;
        public Texture2D Background1Texture;
        public Texture2D TitleScreenTexture;
        public Texture2D LoadingScreenTexture;

        public Texture2D[] MainMenuTextures = new Texture2D[5]; //,s
        //Texture2D[] Start = new Texture2D[3]; //,s
        //Texture2D[] OpScSound = new Texture2D[4]; //,s
        //Texture2D[] OpScFullscreen = new Texture2D[4]; //,s
        //Texture2D[] OpScControls = new Texture2D[6]; //,s
        //Texture2D[] OpScExit = new Texture2D[2]; //,s

        //Animation Textures
        public Texture2D P1NormalStandingTexture;

        //Other Textures
        public Texture2D CrossairTexture;

        //Bullet Textures
        public Texture2D[] BulletTextures;
        #endregion

        #region Objects
        //Objects(Normal)
        public Obj Background1;
        public Obj TitleScreen;
        public Obj LoadingScreen;
        public Obj MainMenuObj;  //,s
        public MovObj Crossair;

        //Objects(Arrays)
        public Player[] Players;
        public Obj[] Guns;
        public Obj[] MeleeWeapons;
        public Obj[] Shields;
        public AniObj[] Traps;
        public InteractiveObjects.HEE[] HEE; //HEE stands for Hostile Enviromental Enemies

        //Objects(List)
        public List<Bullet> Bullets = new List<Bullet>();
        #endregion

        #region Cameras
        public Camera ObservationCamera;
        public Camera[] GameCameras;
        #endregion

        #region Class Systems
        public SplitScreen Splitscreen;
        public SameScreen Samescreen;
        public Debug Debugger;
        //public Network NetworkPlay;
        #endregion

        #region Sounds Effects(Which cannot be loaded in the Audio class)
        #endregion

        #region Music(Which cannot be loaded in the Audio class
        #endregion

        #region Shaders
        public Effect ShaderEffect1;
        #endregion

        //Miscellaneous
        public TileMap Map1 = new TileMap(); //Define a new tile map

        //Here is the instance - It's used for accessing features in the main game class from the other classes
        public static Game1 Instance = null; //It's defined here as nothing...

        public Game1()
        {
            Instance = this; //..to eventually be defined as this class(Game1 class)

            graphics = new GraphicsDeviceManager(this);
            input = new InputManager(Services, Window.Handle); //Define a new input manager

            Content.RootDirectory = "Content";

            Components.Add(input); //Add the input manager as a component

            //See the definitions in the Settings class for descriptions
            graphics.PreferredBackBufferWidth = Settings.DisplayWidth;
            graphics.PreferredBackBufferHeight = Settings.DisplayHeight;
            graphics.PreferMultiSampling = Settings.IngameAntiAliasing;
            graphics.IsFullScreen = Settings.Fullscreen;
            graphics.SynchronizeWithVerticalRetrace = Settings.VerticalSync;
            IsFixedTimeStep = Settings.AlwaysUpdate60FPS;
            IsMouseVisible = Settings.IsMouseVisible;

            Window.Title = Settings.Title;
            Window.AllowUserResizing = Settings.ResizeWindow;

            control = System.Windows.Forms.Form.FromHandle(Instance.Window.Handle); 
            form = control.FindForm();

            if (Settings.BorderlessWindow == true)
                form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            else
                form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            //currentKeyboardState = new KeyboardState(); //Making sure to initialize the keyboard
            //currentMouseState = new MouseState();

            graphics.GraphicsDevice.PresentationParameters.MultiSampleCount = Settings.AntiAliasingLevel; //Set antialiasing level

            Settings.SetDisplayModes(); //Define all the possible display modes that can be used in the game
            Settings.SetSettings(); //Apply the current settings
            Settings.SetRes(new Vector2(graphics.GraphicsDevice.Viewport.Width, graphics.GraphicsDevice.Viewport.Height)); //Set resolution to the current width and height of the screen.

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        /// 

        public void LoadContentOnDemand() 
        {
            ContLoadMang.LoadTileSystem(); //Load in tile system
            ContLoadMang.LoadMainMenu(Content); //Load in main menu
            ContLoadMang.LoadEffects(Content); //Load in graphical effects
            ContLoadMang.LoadBackground(Content); //Load in background
            ContLoadMang.LoadCrossair(Content); //Load in crossair
            ContLoadMang.LoadGuns(Content); //Load in guns
            ContLoadMang.LoadBulletTextures(Content); //Load in bullets
            ContLoadMang.LoadMeleeWeapons(Content); //Load in melee weapons
            ContLoadMang.LoadShields(Content); //Load in shield powerups
            ContLoadMang.LoadTraps(Content); //Load in level-specific traps
            ContLoadMang.LoadHEE(Content); //Load in HEEs
            ContLoadMang.LoadCameras(Content); //Load in cameras
            ContLoadMang.SetSpecificIngameSettings(); //Load in specific ingame settings
            //ContLoadMang.LoadPlayers(Players, Content, 3); //Load in players
            ContLoadMang.LoadPlayers(Content, 3);

            //ContLoadMang.LoadInMovObj(Content, MouseposLocal, Crossair = new MovObj(), "Graphics/HUD/Crossair");
            //collisionTexture = Game1.Instance.Content.Load<Texture2D>("Graphics/Tile-based Maps/Collision");

            //Load Sound Effects 

            //Load Music

            //Load Classes
            Samescreen = new SameScreen();
            Debugger = new Debug();

            Thread.Sleep(20);
            state = GameState.Ingame; //Sets state after loading is finished
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            ContLoadMang.LoadLoadingScreen(Content);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (Loading)
            {
                LoadContentOnDemand();
                Loading = false;
            }

            GamePadState pad1 = Microsoft.Xna.Framework.Input.GamePad.GetState(PlayerIndex.One);
            currentKeyboardState = Keyboard.GetState();
            currentMouseState = Mouse.GetState();

            // Allows the game to exit
            //if (Microsoft.Xna.Framework.Input.GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            //    this.Exit();
            if (input.GetGamePad(PlayerIndex.One).GetState().Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            //if (currentKeyboardState.IsKeyDown(Keys.D))
            //{
            //}
            if (input.Keyboards.Count < 1)
            {
               
            }

            //input.GetGamePad(

            switch (state)
            {
                case GameState.MainMenu:
                    #region menu
                    
                    //If the player has selected play options
                    if (pad1.IsButtonDown(Buttons.A) && oldPad.IsButtonUp(Buttons.A) || currentKeyboardState.IsKeyDown(Keys.Enter) && oldKeyboardState.IsKeyDown(Keys.Enter))
                    {
                        if (MenuSelect == 0)
                        {
                            state = GameState.Ingame;
                            MediaPlayer.Stop();
                        }

                        if (MenuSelect == 1 && MainMenuObj.GFX != MainMenuTextures[4])
                        {
                            MainMenuObj.GFX = MainMenuTextures[4];
                        }

                        if (MenuSelect == 2)
                        {
                            this.Exit();
                        }

                        if (MenuSelect == 2)
                        {
                            SelectCreds();
                        }
                    }

                    if (pad1.IsButtonDown(Buttons.B) && oldPad.IsButtonUp(Buttons.B) || currentKeyboardState.IsKeyDown(Keys.Escape) && oldKeyboardState.IsKeyDown(Keys.Escape))
                    {
                        if (MenuSelect == 1)
                        {
                            MainMenuObj.GFX = MainMenuTextures[4];
                        }
                    }

                    //up on the menu
                    if (pad1.ThumbSticks.Left.Y > 0 && oldPad.ThumbSticks.Left.Y <= 0)
                    {
                        MenuSelect--;
                    }

                    if (currentKeyboardState.IsKeyDown(Keys.W) && oldKeyboardState.IsKeyUp(Keys.W))
                    {
                        MenuSelect--;
                    }

                    //down on the menu
                    else if (pad1.ThumbSticks.Left.Y < 0 && oldPad.ThumbSticks.Left.Y >= 0)
                    {
                        MenuSelect++;
                    }

                    else if (currentKeyboardState.IsKeyDown(Keys.S) && oldKeyboardState.IsKeyUp(Keys.S))
                    {
                        MenuSelect++;
                    }

                    if (MenuSelect > 3)
                        MenuSelect = 0;
                    else if (MenuSelect < 0)
                        MenuSelect = 3;

                    if (MenuSelect > -1 && MenuSelect < 5)
                        MainMenuObj.GFX = MainMenuTextures[MenuSelect];

                    #endregion
                    break;
                case GameState.Lobby:
                    #region playthegame

                    //HEPP!

                    #endregion
                    break;
                case GameState.Pause:
                    #region pause

                    //WOULD YOU LIEK TO TRY AGAIN

                    #endregion
                    break;
                case GameState.Ingame:
                    #region inthegame
                    
                    //WOULD YOU LIEK TO TRY AGAIN
                    if (SplitScreenMode)
                    {
                        for (int i = 0; i < PlayerActivate.Length; i++)
                        {
                            if (PlayerActivate[i])
                            {
                                GameCameras[i].Update(Players[i].Position); //The number of the camera is set to the player that is activated and which player it is.
                            }
                        }
                    }

                    if (NetworkMode || SameScreenMode)
                    {
                        for (int i = 0; i < Bullets.Count; i++)
                        {
                            Bullets[i].Update(gameTime);

                            if (Bullets[i].Bulletis < 0)
                                Bullets.RemoveAt(i);

                            if (Bullets[i].Position.X <= 0 || Bullets[i].Position.Y <= 0 || Bullets[i].Position.X >= Settings.Tiles.tileMapWidth * Settings.Tiles.tileSize.X || Bullets[i].Position.Y >= Settings.Tiles.tileMapHeight * Settings.Tiles.tileSize.Y)
                            {
                                Bullets.RemoveAt(i);
                                break;
                            }

                            if (Map1.Collision.GetCellIndex(Bullets[i].Position) == 1)
                            {
                                Map1.Collision.SetCellIndex(Bullets[i].Position, 0);
                                Map1.Layers[0].SetCellIndex(Bullets[i].Position, -1);
                                Bullets.RemoveAt(i);
                            }
                        }

                        //GameCameras[0].Update(Players[0].Position);
                        //GameCameras[0].currentpos = currentMousepos;
                        GameCameras[0].currentpos = Players[0].Position;
                        GameCameras[0].Update(GameCameras[0].currentpos);
                        Players[0].Update(gameTime);
                        Crossair.Position = MouseposLocal;
                        //MathHelper.Clamp(GameCameras[0].currentpos.Y, -500, 500);
                        //MathHelper.Clamp(GameCameras[0].currentpos.X, -200, 200);

                        MouseposLocal = new Vector2(currentMouseState.X, currentMouseState.Y);
                        //MouseposWorld = Vector2.Transform(MouseposLocal, Matrix.Invert(GameCameras[0].Transform));
                        //MouseposWorld = MouseposLocal + GameCameras[0].currentpos;
                        MouseposWorld.X = MouseposLocal.X + GameCameras[0].currentpos.X - (Settings.DisplayWidth / 2);
                        MouseposWorld.Y = MouseposLocal.Y + GameCameras[0].currentpos.Y - (Settings.DisplayHeight / 2);
                        
                        if (currentMouseState.ScrollWheelValue > 0)
                        {
                            GameCameras[0].Zoom += 0.01f;
                        }

                        if (currentMouseState.ScrollWheelValue < 0)
                        {
                            GameCameras[0].Zoom -= 0.01f;
                        }

                        if (Game1.Instance.Players.Length > 0)
                        {
                            //Add the positions of the players to the camera from the beginning
                            //if (GameCameras[0].Positions == null)
                            //    GameCameras[0].AcknowledgePlayerPositions(Players);

                            //Array.Sort<Players>
                            //Vector2 MaxPosition = new Vector2(Players[Players.Length].Position.X, Players[Players.Length].Position.Y);
                            //Vector2 MinPosition = new Vector2(Players[0].Position.X, Players[0].Position.Y);
                            //GameCameras[0].Zoom = ((MaxPosition.X + MaxPosition.Y) + (MinPosition.X + MaxPosition.Y)) * 0.5f;

                            GameCameras[0] = GameCameras[0];

                            //Vector2 min = Players[0].Position;
                            //Vector2 max = Players[0].Position;

                            //for (int i = 1; i < Players.Count; i++)
                            //{
                            //    if (Players[i].X < min.X) min.X = Players[i].Position.X;
                            //    else if (Players[i].X > max.X) max.X = Players[i].Position.X;
                            //    if (Players[i].Y < min.Y) min.Y = Players[i].Position.Y;
                            //    else if (Players[i].Y > max.Y) max.Y = Players[i].Position.Y;
                            //}

                            //for (int i = 0; i < Players.Length; i++)
                            //{
                            //    if (Players[i].Position.X < min.X) min.X = Players[i].Position.X;
                            //    else if (Players[i].Position.X > max.X) max.X = Players[i].Position.X;
                            //    if (Players[i].Position.Y < min.Y) min.Y = Players[i].Position.Y;
                            //    else if (Players[i].Position.Y > max.Y) max.Y = Players[i].Position.Y;
                            //}

                            //GameCameras[0].Zoom = ((max.X + max.Y) + (min.X + min.Y)) * 0.01f;
                            //GameCameras[0].Zoom = (Players[0].Position + Players[Players.].Position) * 0.5f;

                            //List<Vector2> xPositions;
                            //List<Vector2> yPositions;

                            //foreach (Player player in Players)
                            //{
                            //    xPositions.Add(player.Position.X);
                            //    yPositions.Add(player.Position.Y);
                            //}

                            //MaxVectors = new Vector2(Max(xPositions), Max(yPositions));
                            //MinVectors = new Vector2(Min(xPositions), Min(yPositions));

                            //minPosition = Vector2(minX, minY);
                            //maxPosition = Vector2(minX, minY);

                            if (DetectDisconnect) //If disconnected, go to the disconnect loop.
                            {
                                //backColor = Color.Black;
                                //gameState = GameState.Disconnected;
                            }
                        }

                        //if (MouseDistanceToLimit < 200)
                        //{
                        //    GameCameras[0].currentpos = currentMousepos;
                        //    GameCameras[0].Update(GameCameras[0].currentpos);
                        //}

                        //else
                        //{
                        //    GameCameras[0].currentpos = currentMousepos - (new Vector2(1, 1));
                        //}

                        //Try to calculate distance between mouse and player and limit it to make sure the mouse doesn�t go everywhere and stays put within a radius
                    }

                    #endregion
                    break;
                case GameState.Settings:
                    #region headsettings

                    //Evswhatr

                    #endregion
                    break;
                case GameState.SettingsGamePlay:
                    #region gameplaysettings

                    //Whatevr

                    #endregion
                    break;
                case GameState.SettingsGraphics:
                    #region graphicalsettings

                    //Whatevr

                    #endregion
                    break;
                case GameState.SettingsAudio:
                    #region audiosettings

                    //Whatevr

                    #endregion
                    break;
                case GameState.Loading:
                    #region loadinggame

                    //Whatevr

                    #endregion
                    break;
                case GameState.InputSetup:
                    #region inputcheck

                    // Allow players to join the game, 
                    // and determine active controllers.
                    // In this example, there is only one player.
                    if (PlayerActivate[0] != true)
                        PlayerActivate[0] = true;

                    // When ready, proceed to the game.
                    //gameState = GameState.Game;

                    #endregion
                    break;
                case GameState.Disconnect:
                    #region controldisonnance

                    // If reconnected, continue to the game.
                    if (!DetectDisconnect)
                    {
                        //Display "You didn't do what? You ain't using Action Replay, are you?"
                        //backColor = Color.CornflowerBlue;
                        //GameState = GameState.Ingame;
                    }

                    // Otherwise, pause the game and display a message.

                    #endregion
                    break;
                default:
                    break;
            }

            //Here are the alternate updates
            if (Settings.IngameAntiAliasing)
            graphics.PreferMultiSampling = true;

            //Here are the control updates
            //oldPad = pad1; //Make sure to register the previously pressed buttons...
            //oldKeyboardState = currentKeyboardState; //...and keys.
            //oldMouseState = currentMouseState; //Don't forget the mouse too!

            //Here are the object updates
            //NONE SO FAR

            //Here are the system updates
            if(Debugger != null) //If debug class isn't null
            Debugger.Update(gameTime); //Update debug class

            base.Update(gameTime); //Update the base
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Blue);

            // TODO: Add your drawing code here
            if (state == GameState.Loading)
            {
                spriteBatch.Begin();
                //LoadingScreen.Draw(spriteBatch);
                spriteBatch.Draw(LoadingScreen.GFX, new Rectangle(0, 0, Settings.DisplayWidth, Settings.DisplayHeight), Color.White);
                spriteBatch.End();

                Loading = true;
            }

            if (state == GameState.Ingame)
            {
                #region Camera SpriteBatch

                if (NetworkMode || SameScreenMode)
                {
                    spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.NonPremultiplied, null, null, null, null, GameCameras[0].Transform); //Draw normal objects

                    //WRITE SPRITEBATCH STUFF HERE
                    //Background1.Draw(spriteBatch);
                    Map1.Draw(spriteBatch);
                    Players[0].Draw(spriteBatch);
                    Crossair.Draw(spriteBatch);
                    //StartMenu.Draw(spriteBatch);

                    for (int i = 0; i < Bullets.Count; i++)
                    {
                        Bullets[i].Scale = 0.1f;
                        Bullets[i].Draw(spriteBatch);
                        Bullets[i].Layer = 1f;
                    }

                    spriteBatch.End();

                    spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, GameCameras[0].Transform); //Draw shader-based objects

                    //ShaderEffect1.CurrentTechnique.Passes[0].Apply(); //Apply shader. NOTE! SHADER IS APPLIED BEFORE DRAWING OBJECTS AND ONLY WORKS WITH (SpriteSortMode.Immediate, BlendState.AlphaBlend) SPRITEBATCH
                    //Map1.Draw(spriteBatch);
                    //Players[0].Draw(spriteBatch);

                    spriteBatch.End();
                }

                if (SplitScreenMode)
                {
                    for (int i = 0; i < GameCameras.Length; i++)
                    {
                        spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.NonPremultiplied, null, null, null, null, GameCameras[i].Transform);

                        //WRITE SPRITEBATCH STUFF HERE

                        spriteBatch.End();
                    }
                }

                #endregion

                #region Static SpriteBatch
                spriteBatch.Begin();

                Debugger.DrawingString(spriteBatch);
                //Crossair.Scale = (float)Settings.DisplayHeight / 900;
                //Crossair.Draw(spriteBatch);

                spriteBatch.End();
                #endregion
            }

            if (state == GameState.MainMenu)
            {
                spriteBatch.Begin();

                MainMenuObj.Scale = (float)Settings.DisplayHeight / 900;
                MainMenuObj.Draw(spriteBatch);
                //StartScreen.Draw(spriteBatch);
                //CreditsScreen.Draw(spriteBatch);

                spriteBatch.End();
            }

            base.Draw(gameTime);
        }

        //public void AcknowledgePlayerPositions(Player[] Players)
        //{
        //    for (int i = 0; i < Players.Length; i++)
        //    {
        //        if (Players.Length <= Positions.Length)
        //        {
        //            Positions[i] = Players[i].Position;
        //        }
        //    }
        //}
    }
}
