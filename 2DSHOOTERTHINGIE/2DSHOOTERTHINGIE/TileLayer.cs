﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    public class TileLayer
    {
        Texture2D TileSet;
        Texture2D image;
        Color[] pixelColor;
        int[,] tileMap;

        //Set level size if size differs between maps.
        public void SetLevelSize()
        {
            Settings.Tiles.tileMapWidth = image.Width;
            Settings.Tiles.tileMapHeight = image.Height;
        }

        //Constructor for tileLayer, requires a colorMap (tile mapping with pixel colors) and a tileset.
        public TileLayer(string ColorMap, string Tileset)
        {
            TileSet = Game1.Instance.Content.Load<Texture2D>(Tileset);
            image = Game1.Instance.Content.Load<Texture2D>(ColorMap);
            pixelColor = new Color[image.Width * image.Height]; // pixelColor array that is holding all pixel in the image
            image.GetData<Color>(pixelColor); // Save all the pixels in image to the array pixelColor

            tileMap = new int[image.Height, image.Width];
            #region SET TILE VALUES IN NUMBERS

            int counter = 0;
            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    if (pixelColor[counter] == Color.LightGreen) //Tile 1
                    {
                        tileMap[y, x] = 0;
                    }

                    else if (pixelColor[counter] == Color.SandyBrown) //Tile 2
                    {
                        tileMap[y, x] = 1;
                    }
                    else if (pixelColor[counter] == Color.Gray) //Tile 3
                    {
                        tileMap[y, x] = 2;
                    }
                    else if (pixelColor[counter] == Color.Green) //Tile 4
                    {
                        tileMap[y, x] = 3;
                    }

                    else if (pixelColor[counter] == Color.SaddleBrown) //Tile 5
                    {
                        tileMap[y, x] = 4;
                    }
                    else if (pixelColor[counter] == Color.LightGray) //Tile 6
                    {
                        tileMap[y, x] = 5;
                    }
                    else
                        tileMap[y, x] = -1;

                    //More tiles can be added if needed.


                    counter++;
                }
            }

            #endregion
        }

        //Change the map cell index to a specific value. 
        //TilePosition is the value in tiles that is going to be changed.
        //CellIndex is the value to change to.

        public void SetCellIndex(Point TilePosition, int CellIndex)
        {
            tileMap[TilePosition.Y, TilePosition.X] = CellIndex;
        }

        public void SetCellIndex(Vector2 Position, int CellIndex)
        {
            tileMap[(int)(Position.Y / Settings.Tiles.tileSize.Y), (int)(Position.X / Settings.Tiles.tileSize.X)] = CellIndex;
        }

        public Rectangle getTilePosition(int position)
        {
            int x = position % Settings.Tiles.tileSetSize.X; //if position is outside of the length of the row it will give us the restvalue for ex. 9 % 8 = 1
            int y = position / Settings.Tiles.tileSetSize.X; //calculate the row it's at

            return new Rectangle(x * Settings.Tiles.tileSize.X, y * Settings.Tiles.tileSize.Y, Settings.Tiles.tileSize.X, Settings.Tiles.tileSize.Y);
        }


        public int GetCellIndex(Vector2 Position)
        {
            return tileMap[(int)(Position.Y / Settings.Tiles.tileSize.Y), (int)(Position.X / Settings.Tiles.tileSize.X)];
        }

        public int GetCellIndex(Point TilePosition)
        {
            return tileMap[TilePosition.Y, TilePosition.X];
        }


        #region Draw for Layers

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < Settings.Tiles.tileMapWidth; x++)
            {
                for (int y = 0; y < Settings.Tiles.tileMapHeight; y++)
                {
                    int textureIndex = tileMap[y, x];

                    if (textureIndex == -1)
                        continue;

                    spriteBatch.Draw(TileSet, new Vector2(x * Settings.Tiles.tileSize.X, y * Settings.Tiles.tileSize.Y), getTilePosition(textureIndex), Color.White);
                }
            }
        }

        #endregion

    }
}

       
