﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    public class CollisionLayer
    {
        int[,] tileMap;
        Texture2D image;
        Color[] pixelColor;

        public CollisionLayer(int[,] TileMap)
        {
            tileMap = TileMap;
        }

        public CollisionLayer(string CollisionImg)
        {
            image = Game1.Instance.Content.Load<Texture2D>(CollisionImg);
            pixelColor = new Color[image.Width * image.Height]; // pixelColor array that is holding all pixel in the image
            image.GetData<Color>(pixelColor); // Save all the pixels in image to the array pixelColor

            tileMap = new int[image.Height, image.Width];

            int counter = 0;
            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    if (pixelColor[counter] == Color.LightGreen)
                    {
                        tileMap[y, x] = 1;
                    }

                    else
                    {
                        tileMap[y, x] = 0;
                    }

                    counter++;
                }
            }
        }


        public void SetCellIndex(Point TilePosition, int CellIndex)
        {
            tileMap[TilePosition.Y, TilePosition.X] = CellIndex;
        }

        public void SetCellIndex(Vector2 Position, int CellIndex)
        {
            tileMap[(int)(Position.Y / Settings.Tiles.tileSize.Y), (int)(Position.X / Settings.Tiles.tileSize.X)] = CellIndex;
        }

        public int GetCellIndex(Vector2 Position)
        {
            if (Position.X > 0 && Position.Y > 0 && Position.X < Settings.Tiles.tileMapWidth * Settings.Tiles.tileSize.X && Position.Y < Settings.Tiles.tileMapHeight * Settings.Tiles.tileSize.Y)
            {
                return tileMap[(int)(Position.Y / Settings.Tiles.tileSize.Y), (int)(Position.X / Settings.Tiles.tileSize.X)];
            }
            else return 0;
        }

        public int GetCellIndex(Point TilePosition)
        {
            return tileMap[TilePosition.Y, TilePosition.X];
        }
    }
}
