﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lidgren.Network;

namespace _2DSHOOTERTHINGIE
{
    class Network
    {
        // Client Object
        static NetClient Client;

        // Clients list of characters
        static List<Character> GameStateList;

        // Create timer that tells client, when to send update
        static System.Timers.Timer update;
        
        // Indicates if program is running
        static bool IsRunning;

        // Ask for IP
        string PleaseEnterIP;

        // Read Ip to string
        string hostip = Console.ReadLine();

        // Create new instance of configs. Parameter is "application Id". It has to be same on client and server.
        NetPeerConfiguration Config = new NetPeerConfiguration("game");

        // Create new outgoing message
        NetOutgoingMessage outmsg = Client.CreateMessage();

        public Network()
        {
            if (Game1.NetworkMode)
                IsRunning = true;

            // Create new client, with previously created configs
            Client = new NetClient(Config);

            PleaseEnterIP = "Enter IP To Connect";

            if (IsRunning == true)
            {
                Client.Start();

                //LoginPacket lp = new LoginPacket("Katu");

                // Write byte ( first byte informs server about the message type ) ( This way we know, what kind of variables to read )
                outmsg.Write((byte)PacketTypes.LOGIN);

                // Write String "Name" . Not used, but just showing how to do it
                outmsg.Write("MyName");

                // Connect client, to ip previously requested from user 
                Client.Connect(PleaseEnterIP, 14242, outmsg);

                Console.WriteLine("Client Started");

                // Create the list of characters
                GameStateList = new List<Character>();

                // Set timer to tick every 50ms
                update = new System.Timers.Timer(50);

                // When time has elapsed ( 50ms in this case ), call "update_Elapsed" funtion
                update.Elapsed += new System.Timers.ElapsedEventHandler(update_Elapsed);

                // Funtion that waits for connection approval info from server
                WaitForStartingInfo();

                // Start the timer
                update.Start();
            }
        }

        public void Update(GameTime gametime)
        {
            // While..running
            while (IsRunning)
            {
                // Just loop this like madman
                GetInputAndSendItToServer();
            }
        }

        /// <summary>
        /// Every 50ms this is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void update_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // Check if server sent new messages
            CheckServerMessages();
            
            // Draw the world
            DrawGameState();
        }

        // Before main looping starts, we loop here and wait for approval message
        private static void WaitForStartingInfo()
        {
            // When this is set to true, we are approved and ready to go
            bool CanStart = false;

            // New incomgin message
            NetIncomingMessage inc;

            // Loop untill we are approved
            while (!CanStart)
            {
                // If new messages arrived
                if ((inc = Client.ReadMessage()) != null)
                {
                    // Switch based on the message types
                    switch (inc.MessageType)
                    {
                        // All manually sent messages are type of "Data"
                        case NetIncomingMessageType.Data:
                    
                            // Read the first byte
                            // This way we can separate packets from each others
                            if (inc.ReadByte() == (byte)PacketTypes.WORLDSTATE)
                            {
                                // Worldstate packet structure
                                //
                                // int = count of players
                                // character obj * count



                                //Console.WriteLine("WorldState Update");

                                // Empty the gamestatelist
                                // new data is coming, so everything we knew on last frame, does not count here
                                // Even if client would manipulate this list ( hack ), it wont matter, becouse server handles the real list
                                GameStateList.Clear();

                                // Declare count
                                int count = 0;

                                // Read int
                                count = inc.ReadInt32();

                                // Iterate all players
                                for (int i = 0; i < count; i++)
                                {

                                    // Create new character to hold the data
                                    Character ch = new Character();
                                    
                                    // Read all properties ( Server writes characters all props, so now we can read em here. Easy )
                                    inc.ReadAllProperties(ch);

                                    // Add it to list
                                    GameStateList.Add(ch);
                                }

                                // When all players are added to list, start the game
                                CanStart = true;
                            }
                            break;

                        default:
                            // Should not happen and if happens, don't care
                            Console.WriteLine(inc.ReadString() + " Strange message");
                            break;
                    }
                }
            }
        }


        /// <summary>
        /// Check for new incoming messages from server
        /// </summary>
        private static void CheckServerMessages()
        {
            // Create new incoming message holder
            NetIncomingMessage inc;

            // While theres new messages
            //
            // THIS is exactly the same as in WaitForStartingInfo() function
            // Check if its Data message
            // If its WorldState, read all the characters to list
            while ((inc = Client.ReadMessage()) != null)
            {
                if (inc.MessageType == NetIncomingMessageType.Data)
                {
                    if (inc.ReadByte() == (byte)PacketTypes.WORLDSTATE)
                    {
                        Console.WriteLine("World State uppaus");
                        GameStateList.Clear();
                        int jii = 0;
                        jii = inc.ReadInt32();
                        for (int i = 0; i < jii; i++)
                        {
                            Character ch = new Character();
                            inc.ReadAllProperties(ch);
                            GameStateList.Add(ch);
                        }
                    }
                }
            }
        }


        // Get input from player and send it to server
        private static void GetInputAndSendItToServer()
        {

            // Enum object
            MoveDirection MoveDir = new MoveDirection();
            
            // Default movement is none
            MoveDir = MoveDirection.NONE;

            // Readkey ( NOTE: This normally stops the code flow. Thats why we have timer running, that gets updates)
            // ( Timers run in different threads, so that can be run, even thou we sit here and wait for input )
            ConsoleKeyInfo kinfo = Console.ReadKey();
            
            // This is wsad controlling system
            if (kinfo.KeyChar == 'w')
                MoveDir = MoveDirection.UP;
            if (kinfo.KeyChar == 's')
                MoveDir = MoveDirection.DOWN;
            if (kinfo.KeyChar == 'a')
                MoveDir = MoveDirection.LEFT;
            if (kinfo.KeyChar == 'd')
                MoveDir = MoveDirection.RIGHT;

            if (kinfo.KeyChar == 'q')
            {

                // Disconnect and give the reason
                Client.Disconnect("bye bye");
                
            }

            // If button was pressed and it was some of those movement keys
            if (MoveDir != MoveDirection.NONE)
            {
                // Create new message
                NetOutgoingMessage outmsg = Client.CreateMessage();

                // Write byte = Set "MOVE" as packet type
                outmsg.Write((byte)PacketTypes.MOVE);

                // Write byte = move direction
                outmsg.Write((byte)MoveDir);

                // Send it to server
                Client.SendMessage(outmsg, NetDeliveryMethod.ReliableOrdered);

                // Reset movedir
                MoveDir = MoveDirection.NONE;
            }

        }

        // Move direction enumerator
        enum MoveDirection
        {
            UP,
            DOWN,
            LEFT,
            RIGHT,
            NONE
        }

        // Drawing Gamescreen
        // First clear console
        // Then draw our 3D world ( Hope you brought your glasses with you ;) )
        private static void DrawGameState()
        {
            Console.Clear();
            Console.WriteLine("###############################################################################");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("#.............................................................................#");
            Console.WriteLine("###############################################################################");
            Console.WriteLine("Move with: WASD");
            Console.WriteLine("Connections status: " + (NetConnectionStatus)Client.ServerConnection.Status);
            // Draw each player to their positions
            foreach (Character ch in GameStateList)
            {
                // Sets manually cursor position in console
                Console.SetCursorPosition(ch.X, ch.Y);

                // Write char that marks player
                Console.Write("@");
            }
        }
    }


    class Character
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string Name { get; set; }
        public NetConnection Connection { get; set; }
        public Character(string name, int x, int y, NetConnection conn)
        {
            Name = name;
            X = x;
            Y = y;
            Connection = conn;
        }
        public Character()
        {
        }
    }

    class NetworkPlayerInfo
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string Name { get; set; }
        public NetConnection Connection { get; set; }

        public NetworkPlayerInfo(string name, int x, int y, NetConnection conn)
        {
            Name = name;
            X = x;
            Y = y;
            Connection = conn;
        }

        public NetworkPlayerInfo()
        {
        }
    }

    enum PacketTypes
    {
        LOGIN,
        MOVE,
        WORLDSTATE
    }

        //NetworkSession networkSession;
        //AvailableNetworkSessionCollection availableSessions;
        //int selectedSessionIndex;
        //PacketReader packetReader = new PacketReader();
        //PacketWriter packetWriter = new PacketWriter();


        //public bool CheckForNetwork()
        //{
        //    return true;
        //}

        //public void CheckForDelay()
        //{
        //}

        //public void PacketSend()
        //{
        //}

        //public void PacketRecieve()
        //{
        //}


}


//NetOutgoingMessage sendMsg = server.CreateMessage();
//sendMsg.Write("Hello");
//sendMsg.Write(42);
 
//server.SendMessage(sendMsg, recipient, NetDeliveryMethod.ReliableOrdered);

//NetIncomingMessage incMsg = server.ReadMessage();
//string str = incMsg.ReadString();
//int a = incMsg.ReadInt32();

//There are five delivery methods available:

//Unreliable --- This is just UDP. Messages can be lost or received more than once. Messages may not be received in the same order as they were sent.
//UnreliableSequenced --- Using this delivery method messages can still be lost; but you're protected against duplicated messages. If a message arrives late; 
//                        that is, if a message sent after this one has already been received - it will be dropped. This means you will never receive "older" data than what you already have received.
//ReliableUnordered ---	This delivery method ensures that every message sent will be eventually received. It does not however guarantee what order they will be received in;
//                      late messages may be delivered before newer ones.
//ReliableSequenced --- This delivery method is similar to UnreliableSequenced; except that it guarantees that SOME messages will be received - if you only send one message - it will be received. 
//                      If you sent two messages quickly, and they get reordered in transit, only the newest message will be received - but at least ONE of them will be received.
//ReliableOrdered --- This delivery method guarantees that messages will always be received in the exact order they were sent. 

//****
//INCOMING MESSAGE TYPES
    //Error - This should never appear, it indicates a corrupt message. 

    //StatusChanged - Contains a single byte which can be cast to a NetConnectionStatus enum, then a human readable string explaining the reason for the change in status. These messages will only be received for your own connection(s); not other connections to the same server (if client). 

    //Data - Contains the data written by a connected sender, available in the SenderConnection property of the message. 

    //UnconnectedData - Contains data written by a host that is not connected, this message type is disabled by default. Sender ip endpoint is available in the SenderEndpoint property of the message. 

    //ConnectionApproval - Contains the data passed to Connect() on the remote side. The application should call Approve() or Deny() on the connection object in SenderConnection of the message. This message type is disabled by default. 

    //Receipt - Not yet implemented. 

    //DiscoveryRequest - See Discovery. 

    //DiscoveryResponse - See Discovery. 

    //VerboseDebugMessage - Contains a single string of diagnostics. Only available in DEBUG builds and disabled by default. 

    //DebugMessage - Contains a single string of diagnostics. Only available in DEBUG builds. 

    //WarningMessage - Contains a single string of diagnostics. 

    //ErrorMessage - Contains a single string of diagnostics. 

    //NatIntroductionSuccess - NAT traversal succeeded. 

    //ConnectionLatencyUpdated - Means the average latency for the connection has been updated 

//****
// Peer/server discovery

//Peer discovery is the process of clients detecting what servers are available. Discovery requests can be made in two ways; locally as a broadcast, which will send a signal to all peers on your subnet. Secondly you can contact an ip address directly and query it if a server is running.

//Responding to discovery requests is done in the same way regardless of how the request is made.

//Here's how to do on the client side; ie. the side which makes a request:

//// Enable DiscoveryResponse messages
//config.EnableMessageType(NetIncomingMessageType.DiscoveryResponse);
 
//// Emit a discovery signal
//Client.DiscoverLocalPeers(14242);

//This will send a discovery signal to your subnet; Here's how to receive the signal on the server side, and send a response back to the client:

//// Enable DiscoveryRequest messages
//config.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
 
//// Standard message reading loop
//NetIncomingMessage inc;
//while ((inc = Server.ReadMessage()) != null)
//{
//    switch (inc.MessageType)
//    {
//        case NetIncomingMessageType.DiscoveryRequest:
 
//            // Create a response and write some example data to it
//            NetOutgoingMessage response = Server.CreateMessage();
//            response.Write("My server name");
 
//            // Send the response to the sender of the request
//            Server.SendDiscoveryResponse(response, inc.SenderEndpoint);
//            break;

//When the response then reaches the client, you can read the data you wrote on the server:

//// Standard message reading loop
//NetIncomingMessage inc;
//while ((inc = Client.ReadMessage()) != null)
//{
//    switch (inc.MessageType)
//    {
//        case NetIncomingMessageType.DiscoveryResponse:
 
//            Console.WriteLine("Found server at " + inc.SenderEndpoint + " name: " + inc.ReadString());
//            break;

//****
//Simulating bad network conditions

//On the internet, your packets are likely to run into all kinds of trouble. They can be delayed and lost and they might even arrive multiple times at the destination. Lidgren has a few options to simulate how your application or game will react when this happens. They are all configured using the NetPeerConfiguration class - these properties exists:

//SimulatedLoss 	This is a float which simulates lost packets. A value of 0 will disable this feature, a value of 0.5f will make half of your sent packets disappear, chosen randomly. Note that packets may contain several messages - this is the amount of packets lost.
//SimulatedDuplicatesChance 	This is a float which determines the chance that a packet will be duplicated at the destination. 0 means no packets will be duplicated, 0.5f means that on average, every other packet will be duplicated.
//SimulatedMinimumLatency, SimulatedRandomLatency 	These two properties control simulating delay of packets in seconds (not milliseconds, use 0.05 for 50 ms of lag). They work on top of the actual network delay and the total delay will be: Actual one way latency + SimulatedMinimumLatency + (randomly per packet 0 to SimulatedRandomLatency seconds)

//It's recommended to assume symmetric conditions and configure server and client with the same simulation settings.

//Simulating bad network conditions only works in DEBUG builds. 

//****
//Unconnected messages

//Normally when using the library you will create a connection and then start sending messages, but you don't have to. Lidgren can also send unconnected messages. This is how:

//NetOutgoingMessage msg = client.CreateMessage();
//msg.Write("Hello stranger!");

//IPEndPoint receiver = new IPEndPoint(NetUtility.Resolve("host.somewhere.com"), 1234);

//client.SendUnconnectedMessage(msg, receiver);

//This will send an unconnected message to host.somewhere.com port 1234. The receiver must be started and have the unconnected data message type enabled, like this:

//config.SetMessageTypeEnabled(NetIncomingMessageType.UnconnectedData, true);

//When a message arrives; the sender can be gotten from msg.SenderEndpoint. 

//****  
//There is an override for SendMessage() which takes an integer called 'sequenceChannel' - this can be used for certain delivery methods, namely UnreliableSequenced, ReliableSequenced and ReliableOrdered.

//The sequence channel is a number between 0 and (NetConstants.NetChannelsPerDeliveryMethod - 1) - currently 31. The reason for this limitation is to reduce the amount of overhead per message. Note that there are this amount of channels per delivery method, not in total.

//Messages sent in a certain sequence channel will be dropped/withheld independently of messages sent in a different sequence channel.

//An example:

//Lets say 'Health' are sent using the delivery method ReliableSequenced.
//This means that if a health message is delayed so that message #7 arrives
//AFTER message #8 has already arrived... #7 will be dropped (since #8 has
//already arrived with fresher information).

//This works well, but things is complicated if we also send 'Ammo' as
//ReliableSequenced. Lets say Ammo is sent in message #7 but message #8,
//containing 'Health' arrives before it - then #7 will be dropped for the
//same reasons as above.

//This is clearly not what we want; #7 still contains the freshest info we
//have on ammo.

//We solve this by using different sequence channels for health and ammo
//messages. Message numbers are individual per sequence channel, so health
//and ammo message cannot clash.

//Code:

//// create a message
//NetOutgoingMessage om = peer.CreateMessage();
//om.Write("Hello internet");

//// send it as Reliable Sequenced channel number 8
//peer.SendMessage(om, connection, NetDeliveryMethod.ReliableSequenced, 8);

//****
//Message encryption

//Messages can be encrypted using the Xtea, or other algorithms. This is done like this:

//INetEncryption algo = new NetXtea("TopSecret");

//NetOutgoingMessage msg = client.CreateMessage();
//msg.Write("Some content");
//msg.Write(42);
//msg.Encrypt(algo);

//Notice that encryption must be done last and that after encryption no more data should be written to the message.

//Decrypting the message on arrival is done like this:

//INetEncryption algo = new NetXtea("TopSecret");

//incomingMessage.Decrypt(algo);

//The algorithm instance can be reused over the application lifetime, but it is not thread safe; so you'll have to create an instance per thread if you're encrypting/decrypting on more than one thread.

//Note that this feature does not concern itself with synchronizing the password between hosts and it only encrypts the content of the message, not message headers.

//Currently Xtea, AES, DES, Triple DES, RC2 and a simple XOR algorithm are implemented; it's easy to extend; any class implementing the
//INetEncryption can be used. For block level algorithms there is a base class, NetBlockEncryption, that can be used to simplify such ciphers
//- you will only need to provide implementations for EncryptBlock, DecryptBlock and BlockSize. 

//****
//Recieving messages
//There are three different ways of consuming messages received by the library.

//1. Polling

//If you already have an application loop for your game this would be the most suitable to use; just call ReadMessage() until it returns null.

//NetIncomingMessage msg;
//while ((msg = peer.ReadMessage()) != null)
//{
//   // process message here
//}

//2. Callback

//If your application is completely event/UI-driven and does not have an application loop per se, you can register a callback to fire when a message arrives. The registration must happen from the correct SynchronizationContext (ie. call RegisterReceivedCallback from the same thread you want to receive the message notification on)

//peer.RegisterReceivedCallback(new SendOrPostCallback(GotMessage)); 

//public static void GotMessage(object peer)
//{
//        var msg = peer.ReadMessage();
//        // process message here
//}       

//3. EventWaitHandle

//If your incoming message processing is run on a separate thread you can use the MessageReceivedEvent property to have your thread block until an incoming message arrives.

//// in your separate thread
//peer.MessageReceivedEvent.WaitOne(); // this will block until a message arrives

//var msg = peer.ReadMessage();
//// process message here

