﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RectangleF = System.Drawing.RectangleF;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    public class MovObj : Obj
    {
        public Point CurrentFrame; //This decides the current frame of the sprite.
        public Point FrameSize; //This decides how big the sprite is.
        public Point TotalFrames; //The first value of this decides how many vertical frames there is, and the second value decides how many horizontal frames there is. 

        public Vector2 Direction; //This decides what direction the player is going at.
        public Vector2 OldDirection; //This vector is used for saving the value of a previously used direction

        public float VelocityX; //This decides the horizontal speed of the player
        public float VelocityY; //This decides the vertical speed of the player

        public float MaxVelocityX; //Decides the maximum speed value of an object moving in a vertical direction
        public float MaxVelocityY; //Decides the maximum speed value of an object moving in a horizontal direction

        public float XVelAdd; //The addition to the maximal horisontal velocity of the player
        public float YVelAdd; //The addition to the maximal vertical velocity of the player

        public float Gravity; //The gravitational value - Used for calculating gravity for the player.

        public bool AffectedByGravity; //Detects whether an moving object is affected by gravity or not.

        public virtual void Update(GameTime gameTime)
        {
            if (AffectedByGravity == true)
            {
                VelocityY += Gravity * (float)gameTime.ElapsedGameTime.TotalSeconds;

                MaxVelocityX = MaxVelocityX + XVelAdd; //The vertical max velocity can be extended by adding a certain value. This value is zero by default.
               // MaxVelocityY = MaxVelocityY + YVelAdd; //The horizontal max velocity can be extended by adding a certain value. This value is zero by default.

                if (VelocityX >= MaxVelocityX) //If the velocity is higher than or equal to the set max velocity value...
                    VelocityX = MaxVelocityX; //...set the velocity to the max velocity(This means it's only set to max velocity as long as it exceeds the max velocity value)
                //if (VelocityY >= MaxVelocityY) //Dito explanation for the vertical max velocity.
                //    VelocityY = MaxVelocityY;

                Position.Y += VelocityY * (float)gameTime.ElapsedGameTime.TotalSeconds;
                Position.X += VelocityX * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }

            if (AffectedByGravity == false)
            {
                Position.X += Direction.X * VelocityX; //The horizontal position of a moving game object is decided by the horizontal direction, multiplied with a certain value of speed.
                Position.Y += Direction.Y * VelocityY; //The vertical position of a moving game object is decided by the vertical direction, multiplied with a certain value of speed.
            }
        }

        public virtual RectangleF RectangusF() //A float rectange - Intention is maintaining an accurate collision box for a scaling object.
        {
            return new RectangleF(Position.X, Position.Y, GFX.Width * Scale, GFX.Height * Scale);
        }
    }
}
