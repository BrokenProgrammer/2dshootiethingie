﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    public class ContLoadMang //DO NOT FORGET TO PROGRAM IN THAT CONTLOADMANAGER SHOULD ULTILIZE A FUNCTION CHECKING THE CURRENT ONLINE STATE OF THE GAME
    {
        //This class(ContentLoadManager) is used for dynamically loading and unloading content in the game. 
        //Content.Load(stuff 'n textures)
        //Content.Dispose(textures 'n stuff) - THIS WILL UNLOAD ALL THE CURRENTLY LOADED CONTENT!!!
        //Object.Dispose() - Used for disposing of certain object loaded into the game. 

        public static Texture2D HEPP;
        public static Obj Objuct;

        public void UnloadMenuContentAndLoadLobby()
        {
        }

        public void UnloadLobbyAndLoadLevel()
        {
            //Load selected level

            //DON'T FORGET DETECTION IF NETWORK OR SAMESCREEN OR SPLITSCREEN!
        }

        public void UnloadLevelAndLoadAnotherLevel()
        {
        }

        public void UnloadLevelAndLoadLobby()
        {
        }

        public void UnloadLevelAndLoadMenuContent()
        {
        }

        public void UnloadLobbyAndLoadMenuContent()
        {
        }

        public void LoadPlayer(Player[] player)
        {
        }

        public static void LoadTileSystem()
        {
            Game1.Instance.Map1.Layers.Add(new TileLayer("Graphics/Tile-based Maps/Testmap", "Graphics/Tiles/GrassSandStoneTiles")); //Load in the map and tiles of tilemap
            Game1.Instance.Map1.Layers[0].SetLevelSize(); //Set size of tilemap

            Game1.Instance.Map1.Collision = new CollisionLayer("Graphics/Tile-based Maps/Testmap"); //Load in collision map for tile map
        }

        public static void LoadPlayers(ContentManager contentManager, int numberofPlayers)
        {
            if (Game1.Instance.Players == null)
                Game1.Instance.Players = new Player[numberofPlayers];

            //if (players[0] != null)
            //{
            //    int n = players.Length + 1;

            //    if (n <= players.Length)
            //    {
            //        players[n] = new Player(n);
            //        players[n].GFX = contentManager.Load<Texture2D>("Graphics/Sprites/Player/Sprite1/NormalStanding/af2");
            //        players[n].VelocityX = 0;
            //        players[n].FrameSize = new Point(players[n].FrameSizeX, players[n].FrameSizeY);
            //        players[n].TotalFrames = new Point(8, 8);
            //        players[n].FPS = 24;
            //        players[n].ChangeFrame = 0;
            //        players[n].Position = new Vector2(250 * n, 250);
            //    }
            //}

            if (Game1.SameScreenMode == true || Game1.SplitScreenMode == true)
            {
                for (int i = 0; i < Game1.Instance.Players.Length; i++)
                {
                    Game1.Instance.Players[i] = new Player(i);
                    Game1.Instance.Players[i].GFX = contentManager.Load<Texture2D>("Graphics/Sprites/Player/Sprite1/NormalStanding/af2");
                    Game1.Instance.Players[i].VelocityX = 0;
                    Game1.Instance.Players[i].FrameSize = new Point(Game1.Instance.Players[i].FrameSizeX, Game1.Instance.Players[i].FrameSizeY);
                    Game1.Instance.Players[i].TotalFrames = new Point(8, 8);
                    Game1.Instance.Players[i].FPS = 24;
                    Game1.Instance.Players[i].ChangeFrame = 0;
                    Game1.Instance.Players[i].Position = new Vector2(250 * i, 250);
                    //Players[0].Position = new Vector2(200, 1000);
                }
            }

            if (Game1.NetworkMode == true)
            {
                Game1.Instance.Players[0] = new Player(0);
                Game1.Instance.Players[0].GFX = contentManager.Load<Texture2D>("Graphics/Sprites/Player/Sprite1/NormalStanding/af2");
                Game1.Instance.Players[0].VelocityX = 0;
                Game1.Instance.Players[0].FrameSize = new Point(Game1.Instance.Players[0].FrameSizeX, Game1.Instance.Players[0].FrameSizeY);
                Game1.Instance.Players[0].TotalFrames = new Point(8, 8);
                Game1.Instance.Players[0].FPS = 24;
                Game1.Instance.Players[0].ChangeFrame = 0;
                Game1.Instance.Players[0].Position = new Vector2(250, 250);
            }
        }    

        public static void LoadPlayers(Player[] players, ContentManager contentManager, int numberofPlayers)
        {
            //if (Game1.Instance.Players == null)
            //Game1.Instance.Players = new Player[numberofPlayers];
            if (players == null)
                players = new Player[numberofPlayers];

            //if (players[0] != null)
            //{
            //    int n = players.Length + 1;

            //    if (n <= players.Length)
            //    {
            //        players[n] = new Player(n);
            //        players[n].GFX = contentManager.Load<Texture2D>("Graphics/Sprites/Player/Sprite1/NormalStanding/af2");
            //        players[n].VelocityX = 0;
            //        players[n].FrameSize = new Point(players[n].FrameSizeX, players[n].FrameSizeY);
            //        players[n].TotalFrames = new Point(8, 8);
            //        players[n].FPS = 24;
            //        players[n].ChangeFrame = 0;
            //        players[n].Position = new Vector2(250 * n, 250);
            //    }
            //}

            if (Game1.SameScreenMode == true || Game1.SplitScreenMode == true)
            {
                for (int i = 0; i < players.Length; i++)
                {
                    players[i] = new Player(i);
                    players[i].GFX = contentManager.Load<Texture2D>("Graphics/Sprites/Player/Sprite1/NormalStanding/af2");
                    players[i].VelocityX = 0;
                    players[i].FrameSize = new Point(players[i].FrameSizeX, players[i].FrameSizeY);
                    players[i].TotalFrames = new Point(8, 8);
                    players[i].FPS = 24;
                    players[i].ChangeFrame = 0;
                    players[i].Position = new Vector2(250 * i, 250);
                    //Players[0].Position = new Vector2(200, 1000);
                }
            }

            if (Game1.NetworkMode == true)
            {
                players[0] = new Player(0);
                players[0].GFX = contentManager.Load<Texture2D>("Graphics/Sprites/Player/Sprite1/NormalStanding/af2");
                players[0].VelocityX = 0;
                players[0].FrameSize = new Point(players[0].FrameSizeX, players[0].FrameSizeY);
                players[0].TotalFrames = new Point(8, 8);
                players[0].FPS = 24;
                players[0].ChangeFrame = 0;
                players[0].Position = new Vector2(250, 250);
            }
        }

        //if (SameScreenMode == true || SplitScreenMode == true)
        //for (int i = 0; i < Players.Length; i++)
        //{
        //    Players[i] = new Player(i);
        //    Players[i].GFX = P1NormalStandingTexture;
        //    Players[i].VelocityX = 0;
        //    Players[i].FrameSize = new Point(Players[0].FrameSizeX, Players[0].FrameSizeY);
        //    Players[i].TotalFrames = new Point(8, 8);
        //    Players[i].FPS = 24;
        //    Players[i].ChangeFrame = 0;
        //    //Players[i].Position = new Vector2(250 * i, 250);
        //    Players[0].Position = new Vector2(200, 1000);
        //}

        //if (NetworkMode == true)
        //{
        //    Players[0] = new Player(0);
        //    Players[0].GFX = P1NormalStandingTexture;
        //    Players[0].VelocityX = 0;
        //    Players[0].FrameSize = new Point(Players[0].FrameSizeX, Players[0].FrameSizeY);
        //    Players[0].TotalFrames = new Point(8, 8);
        //    Players[0].FPS = 24;
        //    Players[0].ChangeFrame = 0;
        //    Players[0].Position = new Vector2(250, 250);
        //}

        /// <summary>
        /// Add a single new player to the game
        /// </summary>
        /// <param name="contentManager"></param>
        public static void AddPlayer(ContentManager contentManager)
        {
            if (Game1.Instance.Players.Length >= 1)
            {
                int n = Game1.Instance.Players.Length + 1;
                Game1.Instance.Players[n] = new Player(n);
                Game1.Instance.Players[n].GFX = contentManager.Load<Texture2D>("Graphics/Sprites/Player/Sprite1/NormalStanding/af2");
                Game1.Instance.Players[n].VelocityX = 0;
                Game1.Instance.Players[n].FrameSize = new Point(Game1.Instance.Players[0].FrameSizeX, Game1.Instance.Players[0].FrameSizeY);
                Game1.Instance.Players[n].TotalFrames = new Point(8, 8);
                Game1.Instance.Players[n].FPS = 24;
                Game1.Instance.Players[n].ChangeFrame = 0;
                Game1.Instance.Players[n].Position = new Vector2(250 * n, 250);
            }
        }

        /// <summary>
        /// Unload all the content in the content manager.
        /// </summary>
        /// <param name="content"></param>
        public static void UnloadContentManager(ContentManager content)
        {
            content.Unload();
        }

        /// <summary>
        /// Quit the game. Whaddya expect? THE GAME? HUH?
        /// </summary>
        public static void QuitTheGame()
        {
            Game1.Instance.Exit();
        }

        //

        // 

        //

        /// <summary>
        /// Load the loading screen into the content manager.
        /// </summary>
        /// <param name="content"></param>
        public static void LoadLoadingScreen(ContentManager content)
        {
            Game1.Instance.LoadingScreenTexture = content.Load<Texture2D>("Graphics/Loading");
            Game1.Instance.LoadingScreen = new Obj();
            Game1.Instance.LoadingScreen.GFX = Game1.Instance.LoadingScreenTexture;
            Game1.Instance.LoadingScreen.Position = new Vector2(0, 0);
        }

        /// <summary>
        /// Load the main menu into the game.
        /// </summary>
        /// <param name="content"></param>
        public static void LoadMainMenu(ContentManager content)
        {
            Game1.Instance.MainMenuObj = new Obj();
            Game1.Instance.MainMenuObj.Position = new Vector2(Settings.DisplayWidth / 2, Settings.DisplayHeight / 2);

            Game1.Instance.MainMenuTextures[0] = content.Load<Texture2D>("Graphics/Menu Stuff/MonsterSmasher1"); //s
            Game1.Instance.MainMenuTextures[1] = content.Load<Texture2D>("Graphics/Menu Stuff/MonsterSmasher2"); //s
            Game1.Instance.MainMenuTextures[2] = content.Load<Texture2D>("Graphics/Menu Stuff/MonsterSmasher3");//s
            Game1.Instance.MainMenuTextures[3] = content.Load<Texture2D>("Graphics/Menu Stuff/MonsterSmasher4");//s
            Game1.Instance.MainMenuTextures[4] = content.Load<Texture2D>("Graphics/Menu Stuff/Credits");//s

            Game1.Instance.MainMenuObj.GFX = Game1.Instance.MainMenuTextures[3];

            //PauseScreen.GFX = Content.Load<Texture2D>("pause"); //s
            //gameoverscreen.GFX = Content.Load<Texture2D>("Gameover"); //s
            //PauseScreen = new menu(); //s
        }

        public static void LoadCrossair(ContentManager content)
        {
            Game1.Instance.CrossairTexture = content.Load<Texture2D>("Graphics/HUD/Crossair");
            Game1.Instance.Crossair = new MovObj();
            Game1.Instance.Crossair.GFX = Game1.Instance.CrossairTexture;
            Game1.Instance.Crossair.Position = Game1.Instance.MouseposLocal;
        }

        public static void LoadBackground(ContentManager content)
        {
            Game1.Instance.Background1Texture = content.Load<Texture2D>("Graphics/Menu Stuff/Title Screen");
            Game1.Instance.Background1 = new Obj();
            Game1.Instance.Background1.GFX = Game1.Instance.Background1Texture;
            Game1.Instance.Background1.Position = new Vector2(0, 0);
        }

        public static void LoadEffects(ContentManager content)
        {
            Game1.Instance.ShaderEffect1 = content.Load<Effect>("Graphics/Shaders/Effect1");
        }

        public static void LoadBulletTextures(ContentManager content)
        {
            Game1.Instance.BulletTextures = new Texture2D[21];
            //for (int i = 0; i < BulletTextures.Length; i++)
            //{
            //BulletTextures[i] = Content.Load<Texture2D>("Graphics/Bullets/Bullet1");
            //}
            Game1.Instance.BulletTextures[0] = content.Load<Texture2D>("Graphics/Bullets/Bullet1");
        }

        public static void LoadGuns(ContentManager content)
        {
            Game1.Instance.Guns = new Obj[10];
            for (int i = 0; i < Game1.Instance.Guns.Length; i++)
            {
                Game1.Instance.Guns[i] = new Obj();
            }
        }

        public static void LoadMeleeWeapons(ContentManager content)
        {
            Game1.Instance.MeleeWeapons = new Obj[10];
            for (int i = 0; i < Game1.Instance.MeleeWeapons.Length; i++)
            {
                Game1.Instance.MeleeWeapons[i] = new Obj();
            }
        }

        public static void LoadShields(ContentManager content)
        {
            Game1.Instance.Shields = new Obj[10];
            for (int i = 0; i < Game1.Instance.Shields.Length; i++)
            {
                Game1.Instance.Shields[i] = new Obj();
            }
        }

        public static void LoadTraps(ContentManager content)
        {
            Game1.Instance.Traps = new AniObj[10];
            for (int i = 0; i < Game1.Instance.Traps.Length; i++)
            {
                Game1.Instance.Traps[i] = new AniObj();
                //Traps[i].Gfx = this.Content.Load<Texture2D>("Enemies/BOSS/BUUUUT," + AngryHouseHolder[i].BossLife);
                //Traps[i].Position = new Vector2(-4000, -4000);
                //Traps[i].OriginalXPosition = (int)Traps[i].Position.X;
                //Traps[i].OriginalYPosition = (int)Traps[i].Position.Y;
                //Traps[i].FrameSize = new Point(883, 883);
            }
        }

        public static void LoadHEE(ContentManager content)
        {
            Game1.Instance.HEE = new InteractiveObjects.HEE[10];
            for (int i = 0; i < Game1.Instance.HEE.Length; i++)
            {
                Game1.Instance.HEE[i] = new InteractiveObjects.HEE();
                //HEE[i].Gfx = this.Content.Load<Texture2D>("Enemies/BOSS/BUUUUT," + AngryHouseHolder[i].BossLife);
                //HEE[i].Position = new Vector2(-4000, -4000);
                //HEE[i].OriginalXPosition = (int)Traps[i].Position.X;
                //HEE[i].OriginalYPosition = (int)Traps[i].Position.Y;
                //HEE[i].FrameSize = new Point(883, 883);
            }
        }

        public static void LoadCameras(ContentManager content)
        {
            Game1.Instance.GameCameras = new Camera[3];
            for (int i = 0; i < Game1.Instance.GameCameras.Length; i++)
            {
                Game1.Instance.GameCameras[i] = new Camera(Game1.Instance.graphics.GraphicsDevice.Viewport);
            }

            //if (NetworkMode || SameScreenMode)
            //{
            Game1.Instance.ObservationCamera = new Camera(Game1.Instance.graphics.GraphicsDevice.Viewport);
            //}
        }

        public static void SetSpecificIngameSettings()
        {
            Game1.Instance.MouseposLocal = new Vector2(0, 0);
        }

        /// <summary>
        /// Load graphical things
        /// </summary>
        /// <param name="content"></param>
        public static void LoadGraphicalThings(ContentManager content)
        {
            ////SpriteBatch targetBatch = new SpriteBatch(GraphicsDevice);
            ////RenderTarget2D target = new RenderTarget2D(GraphicsDevice, 320, 240);
            ////GraphicsDevice.SetRenderTarget(target);

            //////perform draw calls

            //////set rendering back to the back buffer
            ////GraphicsDevice.SetRenderTarget(null);

            //////render target to back buffer
            ////targetBatch.Begin();
            ////targetBatch.Draw(target, new Rectangle(0, 0, GraphicsDevice.DisplayMode.Width, GraphicsDevice.DisplayMode.Height), Color.White);
            ////targetBatch.End();
        }

        /// <summary>
        /// Load in unspecific object(Constructor)
        /// </summary>
        /// <param name="content"></param>
        //public static void LoadInObj(ContentManager content, Vector2 position, Obj obj, string textureName)
        //{
        //    obj = new Obj();
        //    obj.Position = position;
        //    obj.GFX = content.Load<Texture2D>(textureName);
        //}

        /// <summary>
        /// Load in unspecific object(Constructor)
        /// </summary>
        /// <param name="content"></param>
        //public void LoadInMovObj(ContentManager content, Vector2 position, Obj movObj, string textureName)
        //{
        //    movObj = new MovObj();
        //    movObj.Position = position;
        //    movObj.GFX = content.Load<Texture2D>(textureName);
        //}

        //public MovObj newMovObj(ContentManager content, Vector2 position, Obj movObj, string textureName)
        //{
        //    MovObj newMObj = new MovObj();
        //    newMObj.Position = position;
        //    newMObj.GFX = content.Load<Texture2D>(textureName);
        //    return newMObj;
        //}
    }
}