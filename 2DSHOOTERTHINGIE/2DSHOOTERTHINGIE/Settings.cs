﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    class Settings
    {
        #region FUNCTIONS/METHODS

        /// <summary>
        /// Sort the array in consideration to high array values - highest value comes first, lowest value comes last.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="insertFloat"></param>
        public void ArrayMax(float[] array, float insertFloat)
        {
            float tempMaxValue;

            for (int e = 0; e < array.Length - 1; e++)
            {
                for (int i = 0; i < array.Length - 1 - e; i++)
                {
                    if (array[i] > array[i + 1])
                    {
                        float TemporarPlaceMinusOne;
                        float TemporarPlaceOne;

                        TemporarPlaceMinusOne = array[i + 1];
                        TemporarPlaceOne = array[i];

                        array[i + 1] = TemporarPlaceOne;
                        array[i] = TemporarPlaceMinusOne;
                    }
                }
            }

            insertFloat = array[0];
        }

        /// <summary>
        /// Sort the array in consideration to low array values - lowest value comes first, highest value comes last.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="insertFloat"></param>
        public void ArrayMin(float[] array, float insertFloat)
        {
            float tempMinValue;

            for (int i = 0; i < array.Length; i++)
            {
                if (i > (i + 1))
                {
                }
            }

            insertFloat = array[array.Length];
        }

        #endregion

        #region GRAPHICAL SETTINGS - MAIN

        public static int DisplayWidth = 1600; //The width of the screen - correspondent to horisontal resolution
        public static int DisplayHeight = 900; //The height of the screen - correspondent to vertical resolution
        public static bool Fullscreen = false; //Enable or disable fullscreen mode
        public static bool IngameAntiAliasing = true; //Enable or disable anti-aliasing(Smooth out rough edges/pixels on the screen)
        public static int AntiAliasingLevel = 2; //Set the anti-aliasing level to the level you want(2 = AAX2, 4= AAX4, etc)
        public static bool VerticalSync = true; //Enable or disable vertical syncronization
        public static bool ResizeWindow = false; //Enable or disable ability to resize the game window. Only works in windowed mode.
        public static bool AlwaysUpdate60FPS = true; //Enable or disable constant update in 60 times in a second(SAID AMOUNT OF TIMES IS EQUAL TO FRAMERATE OF GAME)
        public static string Title = "OEPEDIUS - BOY THAT BDSM GOD SURE FUCKED IT UP"; //The title of the game. Only visible in windowed mode.
        public static bool BorderlessWindow = false; //Enable or disable borderless windowed mode
        public static bool IsMouseVisible = true; //Show the mouse pointer or not
        //public static bool Widescreen = true;

        /// <summary>
        /// Called when the quality of the anti-aliasing is changed.
        /// </summary>
        public static void ChangeAntiAliasing() //FUNCTION IS CALLED WHEN LEVEL OF ANTI-ALIASING IS ALTERED
        {
            Game1.Instance.graphics.GraphicsDevice.PresentationParameters.MultiSampleCount = Settings.AntiAliasingLevel;
        }

        #endregion

        #region GRAPHICAL SETTINGS - RESOLUTION

        //Resolutions should also be customary, i.e. the player should be able to define the resolution himself
        public string CustomResX = System.String.Empty;
        public string CustomResY = System.String.Empty;

        /// <summary>
        /// Set a custom resolution for the screen
        /// </summary>
        public void SetCustomRes() //Function for setting a custom resolution
        {
            CurrentRes.X = int.Parse(CustomResX);
            CurrentRes.Y = int.Parse(CustomResY);
            DisplayWidth = (int)CurrentRes.X;
            DisplayHeight = (int)CurrentRes.Y;
            Game1.Instance.graphics.PreferredBackBufferWidth = DisplayWidth;
            Game1.Instance.graphics.PreferredBackBufferHeight = DisplayHeight;
            Game1.Instance.graphics.ApplyChanges();
        }

        /// <summary>
        /// Set a pre-defined resolution for the screen
        /// </summary>
        /// <param name="DisplayVectorSize"></param>
        public static void SetRes(Vector2 DisplayVectorSize)
        {
            DisplayWidth = (int)DisplayVectorSize.X;
            DisplayHeight = (int)DisplayVectorSize.Y;
            CurrentRes.X = DisplayVectorSize.X;
            CurrentRes.Y = DisplayVectorSize.Y;
            Game1.Instance.graphics.PreferredBackBufferWidth = DisplayWidth;
            Game1.Instance.graphics.PreferredBackBufferHeight = DisplayHeight;
            Game1.Instance.graphics.ApplyChanges();
        }

        /// <summary>
        /// Determines whether or not the resolution is set to full screen.
        /// </summary>
        public static void SetFullScreen()
        {
            if (Fullscreen == false)
            Fullscreen = true;

            else
            Fullscreen = false;

            Game1.Instance.graphics.IsFullScreen = Settings.Fullscreen;
            Game1.Instance.graphics.ApplyChanges();
        }

        /// <summary>
        /// Revert back to the previous resolution the player had.
        /// </summary>
        public void RevertRes() //Function for reversing a resolution
        {
            CurrentRes = PreviousRes; 
        }

        public static Vector2 CurrentRes = new Vector2(DisplayWidth, DisplayHeight); //The current resolution
        public static Vector2 PreviousRes = new Vector2(CurrentRes.X, CurrentRes.Y); //Used to revert to previous resolution IN CASE the game fucks up/glitches when changing resolution

        //public static Vector2[] Resolutions = new Vector2[GraphicsAdapter.DefaultAdapter.SupportedDisplayModes];
        public static List<DisplayMode> DisplayModeList = new List<DisplayMode>();
        public static Vector2[] Resolutions;

        /// <summary>
        /// Detect the number of available display modes that the game supports
        /// </summary>
        public static void SetDisplayModes()
        {
            foreach (DisplayMode dm in GraphicsAdapter.DefaultAdapter.SupportedDisplayModes)
            {
                DisplayModeList.Add(dm);
            }

            Resolutions = new Vector2[DisplayModeList.Count];
        }

        #endregion

        #region GRAPHICAL SETTINGS - SAMESCREEN
        #endregion

        #region GRAPHICAL SETTINGS - NETWORK
        #endregion

        #region GRAPHICAL SETTINGS - SPLITSCREEN
        #endregion

        #region GAMEPLAY SETTINGS - MAIN

        //WHATEVER

        #endregion

        #region GAMEPLAY SETTINGS - PLAYER

        //WHATEVER

        #endregion

        #region GAMEPLAY SETTINGS - CONTROLLER

        //In case of SplitScreen or SameScreen
        public static bool CanJoinDuringGame;

        public void CheckPlayerInput()
        {
        }

        public void LoadPlayerInput()
        {
        }

        #endregion

        #region AUDIO SETTINGS

        public static float Volume { get; set; } //This determines how the float should get information from the volume class and then tells it how the float should set the volume. The value for maximum volume is 1.0.

        #endregion

        #region TILEMAP SETTINGS

        public static class Tiles
        {
            public static Point tileSetSize = new Point(3, 1);//number of tiles in tileset.png
            public static Point tileSize = new Point(34, 34); // number of pixels of one tile in tileset.png
            public static float tileScaleSize = 2;
            public static int tileMapWidth;
            public static int tileMapHeight;


            //Convert position in pixels to a position in the tileMap.
            public static Point ConvertPositionToCell(Vector2 Position)
            {
                return new Point((int)Position.X / Tiles.tileSize.X, (int)Position.Y / Tiles.tileSize.Y);
            }


            //Converts position in TileMap to rectangle in pixels.
            public static Rectangle CreateRectForCell(Point Cell)
            {
                return new Rectangle(Cell.X * Tiles.tileSize.X, Cell.Y * Tiles.tileSize.Y, Tiles.tileSize.X, Tiles.tileSize.Y);
            }

        }

        #endregion

        #region SETTINGS - DEFINITION

        //public Settings() //DOESN'T WORK
        //{
        //    //AUDIO
        //    MediaPlayer.Volume = 1.0f;

        //    //GRAPHICS
        //    CurrentRes = Resolutions[0]; //Set current display resolution to predefined resolution
        //    DisplayWidth = (int)CurrentRes.X;
        //    DisplayHeight = (int)CurrentRes.Y;
        //    Fullscreen = true;
       
        //    //GAMEPLAY
        //    CanJoinDuringGame = true;
        //}

        #endregion

        #region SETTINGS - INITIALIZATIONS

        public static void SetSettings()
        {
            //AUDIO
            MediaPlayer.Volume = 1.0f;

            //GRAPHICS
            for (int i = 0; i < DisplayModeList.Count; i++)
            {
                Resolutions[i].X = DisplayModeList[i].Width;
                Resolutions[i].Y = DisplayModeList[i].Height;
            }

            //CurrentRes = Resolutions[0]; //Set current display resolution to predefined resolution
            DisplayWidth = (int)CurrentRes.X;
            DisplayHeight = (int)CurrentRes.Y;

            //GAMEPLAY
            CanJoinDuringGame = true;
        }

        #endregion
    }
}
