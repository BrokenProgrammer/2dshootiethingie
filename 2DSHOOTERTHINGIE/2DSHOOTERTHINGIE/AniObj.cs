﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RectangleF = System.Drawing.RectangleF;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    public class AniObj : MovObj
    {
        public bool ActiveCorpse; //Whether or not the object is in corpse mode, A.K.A it can't move.
        public bool Attacking; //Whether or not the object is attacking something
        public bool IsAnimationFrozen; //Determines if the player animation is frozen or not(REMEMBER! MAKE SURE THE PLAYER ANIMATION IS STRUCK ON ONE FRAME IN THIS STATE!)

        public int ChangeFrame; //This integer chooses what kind of spritesheet the animated object will use

        public int FrameSizeX; //The vertical animation framesize
        public int FrameSizeY; //The horizontal animation framesize

        protected float frameLength = 0.5F; //This decides the length of the sprite frames, A.K.A how long a frame will be displayed.
        protected float timer = 0; //This is the timer for the sprite animation code.

        public int FPS //FramesPerSecond
        {
            get { return (int)(1f / frameLength); }
            set { frameLength = (float)Math.Max(1f / (float)value, 0.01F); }
        }

        public bool AniPixelCollision(Texture2D ObjTexture1, Texture2D ObjTexture2, Rectangle ObjRectangle1, Rectangle ObjRectangle2) //This is used for pixel perfect collision FOR animated objects.
        {
            //Create a new color data for each of the textures
            Color[] ColorData1 = new Color[ObjTexture1.Width * ObjTexture1.Height];
            Color[] ColorData2 = new Color[ObjTexture2.Width * ObjTexture2.Height];

            //Get the color data from the textures
            ObjTexture1.GetData<Color>(ColorData1);
            ObjTexture2.GetData<Color>(ColorData2);

            //Create an int that gets the information of the objects sides
            int top, bottom, left, right;

            //Get the greater value of each of the objects sides
            top = Math.Max(ObjRectangle1.Top, ObjRectangle2.Top);
            bottom = Math.Min(ObjRectangle1.Bottom, ObjRectangle2.Bottom);
            left = Math.Max(ObjRectangle1.Left, ObjRectangle2.Left);
            right = Math.Min(ObjRectangle1.Right, ObjRectangle2.Right);

            for (int y = top; y < bottom; y++)//For every horisontal pixel
            {
                for (int x = left; x < right; x++) //For every vertical pixel
                {
                    //Check the combined color values from each of the pixels
                    Color Object1Pixels = ColorData1[(y - ObjRectangle1.Top) * (ObjRectangle1.Width) + (x - ObjRectangle1.Left)];
                    Color Object2Pixels = ColorData2[(y - ObjRectangle2.Top) * (ObjRectangle2.Width) + (x - ObjRectangle2.Left)];

                    if (Object1Pixels.A != 0 && Object2Pixels.A != 0) //If the transparency of the checked pixels isn't equal to zero
                        return true; //Then enable the collision
                }
            }

            return false; //When the pixel checking is over, end it
        }

        public override Rectangle Rectangus() //This is a override of the rectangle described in the GameObj class. It replaces the rectangle from the GameObj class with a new one, but it has the same name.
        {
            return new Rectangle((int)Position.X, (int)Position.Y, FrameSize.X * 2, FrameSize.Y * 2);
        }

        public override RectangleF RectangusF() //This is a override of the float-based rectangle described in the MovObj class.
        {
            return new RectangleF(Position.X, Position.Y, FrameSize.X * 2 * Scale, FrameSize.Y * 2 * Scale);
        }
    }
}
