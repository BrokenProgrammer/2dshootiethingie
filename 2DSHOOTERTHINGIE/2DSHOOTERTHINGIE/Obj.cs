﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    public class Obj
    {
        public Vector2 Position; //This vector decides the actual position of the object
        public Vector2 PreviousPosition; //This integer saves the recently previous value of the object's position 
        public Vector2 OriginalPosition; //This integer saves the value of the object's original vertical position

        public Texture2D GFX; //This texture type decides which texture the object should be using.
        public float Angle; //This float decides the angle of the object
        public bool visible = false; //This bool decides whether or not the object should be visible
        public bool ColorChange; //Used for changing the color of the player/object
        public Random Randomizer = new Random(); //Used for randomizing values

        public float R = 1f; //Determines the red single used as color component(How red the object texture is)
        public float G = 1f; //Determines the green single used as color component(How green the object texture is)
        public float B = 1f; //Determines the blue single used as color component(How blue the object texture is)
        public float Alpha = 1f; //Determines the color transparency of the object texture

        public float Layer = 1f; //This float decides the value of an object's layer depth made from this class.
        public float Scale = 1f; //This float decides the scale of the object

        public float RectW = 1f; //RectW stands for Rectangle Width
        public float RectH = 1f; //RectH stands for Rectangle Height

        public virtual Rectangle Rectangus() //This rectangle is used for collision detection with other objects.
        {
            return new Rectangle((int)Position.X, (int)Position.Y, GFX.Width * (int)RectW, GFX.Height * (int)RectH); //First you write where the collision is, then you write the amount of area it covers.
        }

        public virtual void Draw(SpriteBatch spriteBatch) //This is used for drawing the graphics of the game.
        {
            //REMEMBER: THE FLOATS USED IN THE COLOR CONTRUCTOR ARE SINGLES, WHICH MEANS THE COLOR VALUES ARE BETWEEN 0(BLACK) AND 1(WHITE)!
            spriteBatch.Draw(GFX, Position, null, new Color(R, G, B, Alpha), Angle, new Vector2(GFX.Width / 2, GFX.Height / 2), Scale, SpriteEffects.None, Layer);
        }

        public bool PixelCollision(Texture2D ObjTexture1, Texture2D ObjTexture2, Rectangle ObjRectangle1, Rectangle ObjRectangle2) //This is used for pixel perfect collision.
        {
            //Create a new color data for each of the textures
            Color[] ColorData1 = new Color[ObjTexture1.Width * ObjTexture1.Height];
            Color[] ColorData2 = new Color[ObjTexture2.Width * ObjTexture2.Height];

            //Get the color data from the textures of the colliding objects
            ObjTexture1.GetData<Color>(ColorData1);
            ObjTexture2.GetData<Color>(ColorData2);

            //Create four ints that gets the information of the objects sides
            int top, bottom, left, right;

            //Get the greater value of each of the objects sides
            top = Math.Max(ObjRectangle1.Top, ObjRectangle2.Top);
            bottom = Math.Min(ObjRectangle1.Bottom, ObjRectangle2.Bottom);
            left = Math.Max(ObjRectangle1.Left, ObjRectangle2.Left);
            right = Math.Min(ObjRectangle1.Right, ObjRectangle2.Right);

            for (int y = top; y < bottom; y++)//For every horisontal pixel
            {
                for (int x = left; x < right; x++) //For every vertical pixel
                {
                    //Check the combined color values from each of the pixels
                    Color Object1Pixels = ColorData1[(y - ObjRectangle1.Top) * (ObjRectangle1.Width) + (x - ObjRectangle1.Left)];
                    Color Object2Pixels = ColorData2[(y - ObjRectangle2.Top) * (ObjRectangle2.Width) + (x - ObjRectangle2.Left)];

                    if (Object1Pixels.A != 0 && Object2Pixels.A != 0) //If the transparency of the checked pixels isn't equal to zero
                        return true; //Then enable the collision
                }
            }

            return false; //When the pixel checking is over and there's no collision, then end it
        }

        public void ResetColorChange() //Reset the changing colours to default, make sure it doesn't restart or reappear
        {
            R = 1f; G = 1f; B = 1f; 
            ColorChange = false;
        }

    }
}
