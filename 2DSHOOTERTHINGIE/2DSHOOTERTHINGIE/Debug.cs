﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Nuclex.Input; //NOTE FOR NUCLEX! ANY OTHER INPUTS THAN XBOX 360 CONTROLLERS CAN ONLY BE REACHED BY EXTENDEDPLAYERINDEX!
using Nuclex.Input.Devices; //It's suggested to only use Nuclex for alternate gamepads, please.

namespace _2DSHOOTERTHINGIE
{
    public class Debug 
    {
        float Framerate; //The current framerate of the game

        string[] Scores;
        string[] Errors;
        string[] Warnings;

        private InputManager input = Game1.Instance.input;
        private ControllerDetector controldetect;
        
        SpriteFont Font1;
        Vector2 FontPos;
        Vector2 FontPos2;

        static string tableName = System.String.Empty;
        //static string DThumbsString = Game1.Instance.Dire
        //static string D-DPadString
        //static string DButtonString
        //static string DGamePadString

        string name;
        int score;
        string messageString = "";
        string messageString2 = "0";
        Keys[] oldKeys = new Keys[0];
        Keys[] oldKeys2 = new Keys[0];

        bool ActivateHighScore = false;
        bool ScoreMain = true;
        bool ScoreInputScore = false;

        bool ShowScoreInputScore1 = false;
        bool ShowScoreInputScore2 = false;

        bool ShowErrorScoreInputScore1 = false;
        bool ShowErrorScoreInputScore2 = false;

        bool ShowWhatWeveGot = false;

        public void UpdateButton()
        {
            ActivateHighScore = false;
        }

        public void ChangeSceneForth()
        {
            ScoreMain = false;
            ScoreInputScore = true;
            ShowScoreInputScore1 = true;
        }
        public void ChangeSceneBack()
        {
            ShowScoreInputScore1 = false;
            ShowScoreInputScore2 = false;
            ShowErrorScoreInputScore1 = false;
            ShowErrorScoreInputScore2 = false;
            name = null;
            score = 0;
            ShowWhatWeveGot = false;
            ScoreInputScore = false;
            ScoreMain = true;
            messageString = messageString.Remove(messageString.Length - messageString.Length);
            messageString2 = messageString2.Remove(messageString2.Length - messageString2.Length);
        }

        public void ChangeToNextInputScore()
        {
            ShowScoreInputScore2 = true;
            ShowErrorScoreInputScore1 = false;
            ShowScoreInputScore1 = false;
            name = messageString;
        }

        public void ChangeToNextInputScore2()
        {
            ShowWhatWeveGot = true;
            ShowErrorScoreInputScore2 = false;
            ShowScoreInputScore2 = false;
            score = int.Parse(messageString2);
        }

        //TO LOAD IN GAME CLASS
        //Font1 = Content.Load<SpriteFont>("SpriteFont1");
        //FontPos = new Vector2(graphics.GraphicsDevice.Viewport.Width / 3, graphics.GraphicsDevice.Viewport.Height / 5);

        //FOR UPDATE
         //KeyboardState keyState = Keyboard.GetState();

         //           Keys[] pressedKeys;
         //           pressedKeys = keyState.GetPressedKeys();

         //           for (int i = 0; i < pressedKeys.Length; i++)
         //           {
         //               bool foundIt = false;
         //               for (int j = 0; j < oldKeys.Length; j++)
         //               {
         //                   if (pressedKeys[i] == oldKeys[j])
         //                   {
         //                       foundIt = true;
         //                       break;
         //                   }
         //               }
         //               if (foundIt == false)
         //               {
         //                   string keyString = "";
         //                   switch (pressedKeys[i])
         //                   {
         //                       case Keys.Space:
         //                           keyString = " ";
         //                           break;
         //                       case Keys.OemPeriod:
         //                           keyString = ".";
         //                           break;
         //                       case Keys.Enter:
         //                           keyString = "\n";
         //                           break;
         //                       case Keys.D0:
         //                           keyString = "0";
         //                           break;
         //                       case Keys.D1:
         //                           keyString = "1";
         //                           break;
         //                       case Keys.D2:
         //                           keyString = "2";
         //                           break;
         //                       case Keys.D3:
         //                           keyString = "3";
         //                           break;
         //                       case Keys.D4:
         //                           keyString = "4";
         //                           break;
         //                       case Keys.D5:
         //                           keyString = "5";
         //                           break;
         //                       case Keys.D6:
         //                           keyString = "6";
         //                           break;
         //                       case Keys.D7:
         //                           keyString = "7";
         //                           break;
         //                       case Keys.D8:
         //                           keyString = "8";
         //                           break;
         //                       case Keys.D9:
         //                           keyString = "9";
         //                           break;
         //                       case Keys.RightShift:
         //                           keyString = "";
         //                           break;
         //                       case Keys.LeftShift:
         //                           keyString = "";
         //                           break;
         //                       default:
         //                           keyString = pressedKeys[i].ToString();
         //                           break;
         //                   }

         //                   if (currentKeyboardState.IsKeyDown(Keys.LeftShift) || currentKeyboardState.IsKeyDown(Keys.RightShift))
         //                   {
         //                       keyString = keyString.ToString().ToUpper();
         //                   }
         //                   else
         //                   {
         //                       keyString = keyString.ToString().ToLower();
         //                   }
         //                   if (pressedKeys[i] == Keys.Back && messageString.Length > 0)
         //                   {
         //                       messageString = messageString.Remove(messageString.Length - 1);
         //                   }
         //                   else
         //                   {
         //                       messageString = messageString + keyString;
         //                   }

         //                   oldKeys = pressedKeys; 
        //

        //FOR DRAWUPDATE(REMEMBER: STATIC CAMERA)
        //if (ScoreMain == true)
        //    {
        //        string output = "HERE MAH HIGHSCOAR LISTH K THX BAI";
        //        string info = "Press Q To quit Highscore.";
        //        string info2 = "Press E to put in your own score. Press R to reload the list.";
        //        Vector2 FontOrigin = Font1.MeasureString(output) / 2;
        //        spriteBatch.DrawString(Font1, output, FontPos, Color.Black, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);

        //        for (int i = 0; i < 10; i++)
        //        {
        //            Scores[i] = HSTable.ranks[i] + " " + HSTable.names[i] + " " + HSTable.infos[i] + " " + HSTable.scores[i];
        //            spriteBatch.DrawString(Font1, Scores[i], new Vector2(300, (120 + (i * 20))), Color.Black, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);

        //            if (i >= 9)
        //            {
        //                spriteBatch.DrawString(Font1, info, new Vector2(300, (120 + (i * 20 + 40))), Color.Black, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
        //                spriteBatch.DrawString(Font1, info2, new Vector2(300, (120 + (i * 20 + 60))), Color.Black, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
        //            }
        //        }
        //    }

        //    if (ScoreInputScore == true)
        //    {
        //        string nameplease = "Please write your name.";
        //        string scoreplease = "Please write your score. (Don't make too large though!)";
        //        Vector2 FontOrigin = Font1.MeasureString(nameplease) / 2;

        //        if (ShowScoreInputScore1 == true)
        //        {
        //            spriteBatch.DrawString(Font1, nameplease, new Vector2(300, 120), Color.Black, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
        //            spriteBatch.DrawString(Font1, messageString, new Vector2(300, 140), Color.Red);
        //        }
        //        if (ShowScoreInputScore2 == true)
        //        {
        //            spriteBatch.DrawString(Font1, scoreplease, new Vector2(300, 120), Color.Black, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
        //            spriteBatch.DrawString(Font1, messageString2, new Vector2(300, 140), Color.Red);
        //        }

        //        if (ShowErrorScoreInputScore1 == true)
        //        {
        //            string showerror1 = "YOU SHALL NOT PASS, unless you can prove yourself capable of writing.";
        //            spriteBatch.DrawString(Font1, showerror1, new Vector2(300, 160), Color.Black, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
        //        }
        //        if (ShowErrorScoreInputScore2 == true)
        //        {
        //            string showerror2 = "You sure you're not a virgin? I mean, since you haven't got a score...";
        //            spriteBatch.DrawString(Font1, showerror2, new Vector2(300, 160), Color.Black, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
        //        }

        //        if (ShowWhatWeveGot == true)
        //        {
        //            string showit = "The name you've chosen is " + name + ".";
        //            string showit2 = "The score you've chosen is " + score + ".";
        //            string showit3 = "Please press enter to return back to the original menu.";
        //            spriteBatch.DrawString(Font1, showit, new Vector2(300, 120), Color.Black, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
        //            spriteBatch.DrawString(Font1, showit2, new Vector2(300, 140), Color.Black, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
        //            spriteBatch.DrawString(Font1, showit3, new Vector2(300, 160), Color.Black, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
        //        }
        //    }

        static string DebugConsole; //This allows input/output text, intended for checking debugging values.

        public Debug()
        {
            DebugConsole = System.String.Empty;
            Font1 = Game1.Instance.Content.Load<SpriteFont>("SpriteFont1");
            FontPos = new Vector2((Settings.DisplayWidth / 3), (Settings.DisplayHeight / 5));
            FontPos2 = new Vector2((Settings.DisplayWidth / 3), (Settings.DisplayHeight / 5) + 15);
            //FontPos3 = new Vector2(Settings.DisplayWidth / 3, Settings.DisplayHeight / 5 + 100);
        }

        public void Update(GameTime gameTime)
        {
            KeyboardState keyState = Keyboard.GetState();

            if (Game1.Instance.DebugMode == true)
            {
                Framerate = 1 / (float)gameTime.ElapsedGameTime.TotalSeconds; //Calculate the framerate of the game

                //These commandos are used if the debug function is used.
                //ERRORS

                //Exempel:
                //Om koden inte hittar grafik för ett specifikt objekt
                //Skriv "Error 21: Can't find the graphical image for specified object. Have you made sure the image is properly loaded?"
                //Eller nåt sånt. Väldigt smidigt sätt att hitta problem utan att krångla med XNA-jäveln. Vad tycks?

                //for (int i = 0; i < Errors.Length; i++)
                //{
                //    Errors[0] = "Error 0: More than one display mode is activated. There can only be one display mode activated at a time. Please check if more than one of the display modes are activated";
                //    Errors[1] = "Error 1: Can't find texture for object. Make sure the object and the texture itself is properly loaded into the game before debugging";
                //    Errors[2] = "Error 2: ";
                //}

                //for (int i = 0; i < Warnings.Length; i++)
                //{
                //    Warnings[0] = "Warning 0: The current framerate of the game is below the optimal framerate for the game. Make sure there aren't any processes interrupting the framerate flow.";
                //    Warnings[1] = "Warning 1: The object is outside the offset value. Make sure to check that the code is stable and optimized and that the game has proper framerate!";
                //    Warnings[2] = "Warning 2: More than one animation state is activated for Player X. Only one should be activated at a time for each player - Check the code.";
                //    Warnings[3] = "Warning 3: ";
                //}

                //if (currentKeyboardState.IsKeyDown(Keys.LeftShift) || currentKeyboardState.IsKeyDown(Keys.RightShift))
                //{

                //}
            }
        }

        public virtual void DrawingString(SpriteBatch spriteBatch) //This is used for drawing the debug text - Which is used for checking certain values and if they're correct or not.
        {
            if (Game1.Instance.DebugMode)
            {
                //REMEMBER: THE FLOATS USED IN THE COLOR CONTRUCTOR ARE SINGLES, WHICH MEANS THE COLOR VALUES ARE BETWEEN 0(BLACK) AND 1(WHITE)!
                //spriteBatch.DrawString(Font1, "" + Game1.Instance.GameCameras[0].resscalex + "", new Vector2
                //    (FontPos.X * Game1.Instance.GameCameras[0].resscalex, FontPos.Y * Game1.Instance.GameCameras[0].resscaley), Color.White);
                //spriteBatch.DrawString(Font1, "" + Game1.Instance.GameCameras[0].resscaley + "", new Vector2
                //    (FontPos2.X * Game1.Instance.GameCameras[0].resscalex, FontPos2.Y * Game1.Instance.GameCameras[0].resscaley), Color.White);

                for (int i = 0; i < Game1.Instance.Bullets.Count; i++)
                {
                    spriteBatch.DrawString(Font1, "" + Game1.Instance.Bullets[i].Angle + "", new Vector2
                        (FontPos.X * Game1.Instance.GameCameras[0].resscalex, FontPos.Y * Game1.Instance.GameCameras[0].resscaley), Color.White);
                    spriteBatch.DrawString(Font1, "" + Game1.Instance.MouseposWorld + "", new Vector2
                        (FontPos2.X * Game1.Instance.GameCameras[0].resscalex, FontPos2.Y * Game1.Instance.GameCameras[0].resscaley), Color.White);
                }

                //for (ExtendedPlayerIndex i = ExtendedPlayerIndex.Five; i < ExtendedPlayerIndex.Eight; i++)
                //for (int i = 0; i < input.GamePads.Count; i++)
                //{
                    for (ExtendedPlayerIndex e = ExtendedPlayerIndex.Five; e < ExtendedPlayerIndex.Eight; e++)
                    {
                        spriteBatch.DrawString(Font1, "" + e + "", new Vector2(
                        (Settings.DisplayWidth - 100) * Game1.Instance.GameCameras[0].resscalex, 
                        (Settings.DisplayHeight - 100) * Game1.Instance.GameCameras[0].resscaley),
                        Color.White);
                    }
                //}

                //spriteBatch.DrawString(Font1, "" + Game1.Instance.GameCameras[0].aspectus + "", FontPos3, Color.White);
            }
        }
    }
}
