﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    public class SplitScreen
    {
        public SplitScreen()
        {
            
        }

        public void Update(GameTime gameTime)
        {
        }

        //FUNCTIONS DOWN BELOW ARE FOR TWO PLAYERS
        public void VerticalSplitScreen()
        {
        }

        public void HorisontalSplitScreen()
        {
        }

        //FUNCTIONS DOWN BELOW ARE FOR THREE PLAYERS
        public void TwoLeftOneRight()
        {
        }

        public void TwoRightOneLeft()
        {
        }

        public void TwoUpOneDown()
        {
        }

        public void TwoDownOneUp()
        {
        }

        //FUNCTIONS DOWN BELOW ARE FOR FOUR PLAYERS
        public void MoveToScreensLeft()
        {
        }

        public void MoveToScreensRight()
        {
        }
    }
}
