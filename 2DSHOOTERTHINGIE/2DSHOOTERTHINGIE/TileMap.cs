﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    public class TileMap
    {
        //Create a list that saves all layers.
        public List<TileLayer> Layers = new List<TileLayer>();

        //Saves the collision layer.
        public CollisionLayer Collision;

        //Create a universal draw method that will draw all layers per tileMap.
        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (TileLayer L in Layers)
            {
                L.Draw(spriteBatch);
            }
        }
    }
}
