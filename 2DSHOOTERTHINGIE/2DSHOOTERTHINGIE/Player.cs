﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Nuclex.Input; //NOTE FOR NUCLEX! ANY OTHER INPUTS THAN XBOX 360 CONTROLLERS CAN ONLY BE REACHED BY EXTENDEDPLAYERINDEX!
using Nuclex.Input.Devices; //It's suggested to only use Nuclex for alternate gamepads, please.

namespace _2DSHOOTERTHINGIE
{
    public class Player : AniObj
    {
        #region Variables

        //PLAYER VARS
        public int Life; //How much life the player has
        public int Score; //The total score of the player
        public int Ammo; //The total ammunition of the player
        public int Kills; //How many enemies the player has killed
        public int Deaths; //How many times the player has died
        public int Assistances; //How many kills the player has assisted with
        private int AddRes;

        //MANEUVERABILITY
        public bool CanMove; //Determines if the player can move or not.

        //PLAYER WEAPONS
        public float BulletPower; //Determined by the velocity and the angle of the player and the bullet, the damage of the bullet will change dynamically
        public bool IsBulletFired; //Determines if the player has fired a shot or not

        public float KnifePower; //Determined by the velocity and the angle of the player, the damage of the knife will change dynamically
        public bool HasPlayerStabbed; //Determines if the player has stabbed or not

        public float ExplosivePower; //Determined by which explosive is used, obviously.
        public bool IsExplosiveThrown; //Determines if the player has thrown the explosive;
        public bool IsExplosiveActivated; //Determines whether or not the explosive has been activated.

        public bool[] SelectedWeapon; //Determines which weapon is selected

        //WHAT CONTROLS IS USED
        public bool IsKeyboard; //If the player uses a keyboard or not
        public bool IsGamePad; //If the player uses a Xbox 360 controller or not
        public bool IsAlternateGamePad; //If the player uses a controller that is not a Xbox 360 controller

        //CAMERA
        public Vector2 CameraOffset; //The offset for the camera, since it's focused on the player
        public float CameraOffsetY; //The Y value of the camera offset
        public float CameraOffsetX; //The X value of the camera offset
        public bool ShakeScreen; //Used for shaking the screen
        public float ShakeEffect; //Used for determining how much the screen should shake

        //PLAYER CONDITIONS
        public bool HasJumped; //Check if the player has jumped
        public bool HasLanded; //Check if the player has landed
        public bool HasDied; //Check if the player has died
        public bool IsRespawning; //Check if the player is respawning

        //PLAYER EFFECTS
        public bool GOTTAGOFAST; //FHIS IS A VEWWY VEWWY SECWET EASTA EGGH (Here's a hint: You go really fast)
        public bool HealthBuff; //The player's health is temporarily boosted
        public bool ArmorBuff; //The player's armor is temporarily boosted
        public bool WeaponBuff; //The player's current weapons are temporarily boosted
        public bool JumpBuff; //The player's jumping ability goes thru the skies
        public bool SpeedBuff; //The player's acceleration and velocity is improved
        public bool StabManiac; //The player's stabbing abililty is greatly enhanced

        //PLAYER GUI
        public bool ShowPlayerHealthColor; //Whether the health of the player is shown through the color of his skin or not - Green is fleen and Red is ded
        public bool ShowPlayerHealthBarFollow; //Whether or not the health of the player should be shown by a health bar following the player's position.
        public bool ShowPlayerHealthDefault; //Whether or not the health of the player is shown through the default GUI or not

        public bool ShowPlayerNameFollow; //Whether or not the name of the player is shown following the player's position or not
        public bool ShowPlayerNameDefault; //Whether or not the name of the player is shown through the default GUI or not

        #endregion
        #region TileMap

        Texture2D collisionTexture; //Define the texture of the collision points
        protected Vector2[] CollisionPoints = new Vector2[4]; //Set the number of collision points
        public Vector2 Offset; //Set the position of the collision point offset
        private Vector2 radius;

        public Vector2 Radius //Set the radius of the collision points(REMARK: THIS IS A PROPERTY!!!)
        {
            get { return radius; } //Get the private radius
            set
            {
                radius = value; //Set the public radius as the same value as the private radius --> "value" directs to the public Radius itself

                CollisionPoints[0] = new Vector2(-radius.X, -radius.Y); //Sets the upper left collision point.
                CollisionPoints[1] = new Vector2(radius.X, -radius.Y); //Right upper point.
                CollisionPoints[2] = new Vector2(-radius.X, radius.Y); //Lower left point.
                CollisionPoints[3] = new Vector2(radius.X, radius.Y); //Lower right point.
            }
        }

        #endregion
        #region Input

        private InputManager input = Game1.Instance.input; //Make sure the input manager is the same as in Game1 class

        BufferedKeyboard keyboard; //Gets the state of the buffered keyboard
        BufferedMouse mouse; //Gets the state of the buffered mouse

        PlayerIndex padNumber; //Saves what controller the player is using
        ExtendedPlayerIndex padNumberAlter;
        

        KeyboardState keyboardStateOld, //Saves the old pressed keys
          keyboardStateCurrent; //Saves what keyboard the player is using
        MouseState mouseStateOld, //Saves the old pressed mouse buttons
            mouseStateCurrent; //Saves what mouse the player is using
        GamePadState oldPad; //Saves the old pressed buttons
        ExtendedGamePadState currentPadAlter, oldPadAlter; //Saves the old pressed buttons for alternate controllers

        enum Button //Enum for buttons
        {
            Left, //Make player go left
            Right, //Make player go right
            Up, //Currently unused? Originally used for jump
            Down, //Currently unused? Can be used for
            Jump,
            Interact,
            Reload,
            Fire,
            ChangeWeaponNext,
            ChangeWeaponPrev,
            Start,
        }

        private bool KeyIsDown(Button Key)
        {
            #region KEYBOARD
            if (IsKeyboard)
            {
                keyboardStateCurrent = Keyboard.GetState();

                switch (padNumber)
                {
                    case PlayerIndex.One:
                        if (Key == Button.Left)
                            return keyboardStateCurrent.IsKeyDown(Keys.A);

                        if (Key == Button.Right)
                            return keyboardStateCurrent.IsKeyDown(Keys.D);

                        if (Key == Button.Up)
                            return keyboardStateCurrent.IsKeyDown(Keys.W);

                        if (Key == Button.Down)
                            return keyboardStateCurrent.IsKeyDown(Keys.S);

                        if (Key == Button.Jump)
                            return keyboardStateCurrent.IsKeyDown(Keys.Space);

                        if (Key == Button.Interact)
                            return keyboardStateCurrent.IsKeyDown(Keys.E);

                        if (Key == Button.Reload)
                            return keyboardStateCurrent.IsKeyDown(Keys.R);

                        break;
                    case PlayerIndex.Two:
                        break;
                    case PlayerIndex.Three:
                        break;
                    case PlayerIndex.Four:
                        break;
                    default:
                        break;
                }
            }
            #endregion

            #region GAMEPAD
            if (IsGamePad)
            {
                switch (padNumber)
                {
                    case PlayerIndex.Four:
                        break;
                    case PlayerIndex.One:

                        if (Key == Button.Left)
                            return keyboardStateCurrent.IsKeyDown(Keys.A);

                        if (Key == Button.Right)
                            return keyboardStateCurrent.IsKeyDown(Keys.D);

                        if (Key == Button.Up)
                            return keyboardStateCurrent.IsKeyDown(Keys.W);

                        if (Key == Button.Down)
                            return keyboardStateCurrent.IsKeyDown(Keys.S);

                        if (Key == Button.Jump)
                            return keyboardStateCurrent.IsKeyDown(Keys.Space);

                        if (Key == Button.Interact)
                            return keyboardStateCurrent.IsKeyDown(Keys.E);

                        if (Key == Button.Reload)
                            return keyboardStateCurrent.IsKeyDown(Keys.R);

                        break;
                    case PlayerIndex.Three:
                        break;
                    case PlayerIndex.Two:
                        break;
                    default:
                        break;
                }
            }
            #endregion

            #region ALTERNATEGAMEPAD
            if (IsAlternateGamePad)
            {
                switch (padNumberAlter)
                {
                    case ExtendedPlayerIndex.Five:
                        if (Key == Button.Left)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationX) > 0.1f);

                        if (Key == Button.Right)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationX) < 0.1f);

                        if (Key == Button.Up)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationY) > 0.1f);

                        if (Key == Button.Down)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationY) < 0.1f);

                        if (Key == Button.Jump)
                            return currentPadAlter.IsButtonDown(0);

                        if (Key == Button.Interact)
                            return currentPadAlter.IsButtonDown(1);

                        if (Key == Button.Reload)
                            return currentPadAlter.IsButtonDown(2);

                        if (Key == Button.Fire)
                            return currentPadAlter.IsButtonDown(3);

                        if (Key == Button.ChangeWeaponNext)
                            return currentPadAlter.IsButtonDown(currentPadAlter.Pov1);

                        if (Key == Button.ChangeWeaponPrev)
                            return currentPadAlter.IsButtonDown(currentPadAlter.Pov2);

                        if (Key == Button.Start)
                            return currentPadAlter.IsButtonDown(6);

                        break;

                    case ExtendedPlayerIndex.Six:
                         if (Key == Button.Left)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationX) > 0.1f);

                        if (Key == Button.Right)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationX) < 0.1f);

                        if (Key == Button.Up)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationY) > 0.1f);

                        if (Key == Button.Down)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationY) < 0.1f);

                        if (Key == Button.Jump)
                            return currentPadAlter.IsButtonDown(0);

                        if (Key == Button.Interact)
                            return currentPadAlter.IsButtonDown(1);

                        if (Key == Button.Reload)
                            return currentPadAlter.IsButtonDown(2);

                        if (Key == Button.Fire)
                            return currentPadAlter.IsButtonDown(3);

                        if (Key == Button.ChangeWeaponNext)
                            return currentPadAlter.IsButtonDown(4);

                        if (Key == Button.ChangeWeaponPrev)
                            return currentPadAlter.IsButtonDown(5);

                        if (Key == Button.Start)
                            return currentPadAlter.IsButtonDown(6);

                        break;

                    case ExtendedPlayerIndex.Seven:
                         if (Key == Button.Left)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationX) > 0.1f);

                        if (Key == Button.Right)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationX) < 0.1f);

                        if (Key == Button.Up)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationY) > 0.1f);

                        if (Key == Button.Down)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationY) < 0.1f);

                        if (Key == Button.Jump)
                            return currentPadAlter.IsButtonDown(0);

                        if (Key == Button.Interact)
                            return currentPadAlter.IsButtonDown(1);

                        if (Key == Button.Reload)
                            return currentPadAlter.IsButtonDown(2);

                        if (Key == Button.Fire)
                            return currentPadAlter.IsButtonDown(3);

                        if (Key == Button.ChangeWeaponNext)
                            return currentPadAlter.IsButtonDown(4);

                        if (Key == Button.ChangeWeaponPrev)
                            return currentPadAlter.IsButtonDown(5);

                        if (Key == Button.Start)
                            return currentPadAlter.IsButtonDown(6);

                        break;

                    case ExtendedPlayerIndex.Eight:
                         if (Key == Button.Left)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationX) > 0.1f);

                        if (Key == Button.Right)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationX) < 0.1f);

                        if (Key == Button.Up)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationY) > 0.1f);

                        if (Key == Button.Down)
                            return (currentPadAlter.GetAxis(ExtendedAxes.AccelerationY) < 0.1f);

                        if (Key == Button.Jump)
                            return currentPadAlter.IsButtonDown(0);

                        if (Key == Button.Interact)
                            return currentPadAlter.IsButtonDown(1);

                        if (Key == Button.Reload)
                            return currentPadAlter.IsButtonDown(2);

                        if (Key == Button.Fire)
                            return currentPadAlter.IsButtonDown(3);

                        if (Key == Button.ChangeWeaponNext)
                            return currentPadAlter.IsButtonDown(4);

                        if (Key == Button.ChangeWeaponPrev)
                            return currentPadAlter.IsButtonDown(5);

                        if (Key == Button.Start)
                            return currentPadAlter.IsButtonDown(6);

                        break;

                    default:
                        break;
                }
            }
            #endregion

            return false;
        }

        private bool KeyIsUp(Button Key)
        {
            #region KEYBOARD
            if (IsKeyboard)
            {
                keyboardStateCurrent = Keyboard.GetState();

                switch (padNumber)
                {
                    case PlayerIndex.One:

                        if (Key == Button.Left)
                            return keyboardStateOld.IsKeyUp(Keys.A);

                        if (Key == Button.Right)
                            return keyboardStateOld.IsKeyUp(Keys.D);

                        if (Key == Button.Up)
                            return keyboardStateOld.IsKeyUp(Keys.W);

                        if (Key == Button.Down)
                            return keyboardStateOld.IsKeyUp(Keys.S);

                        if (Key == Button.Jump)
                            return keyboardStateOld.IsKeyUp(Keys.Space);

                        if (Key == Button.Interact)
                            return keyboardStateOld.IsKeyUp(Keys.E);

                        if (Key == Button.Reload)
                            return keyboardStateOld.IsKeyUp(Keys.R);

                        //if (Key == Button.ChangeWeaponNext)
                        //

                        //if (Key == Button.ChangeWeaponPrev)
                        //

                        if (Key == Button.Start)
                            return keyboardStateOld.IsKeyUp(Keys.Enter);

                        //if (Key == Button.Fire)
                        //

                        break;
                    case PlayerIndex.Two:
                        break;
                    case PlayerIndex.Three:
                        break;
                    case PlayerIndex.Four:
                        break;
                    default:
                        break;
                }
            }
            #endregion

            #region GAMEPAD
            if (IsGamePad)
            {
                switch (padNumber)
                {
                    case PlayerIndex.Four:
                        break;
                    case PlayerIndex.One:

                        if (Key == Button.Left)
                            return keyboardStateCurrent.IsKeyDown(Keys.A);

                        if (Key == Button.Right)
                            return keyboardStateCurrent.IsKeyDown(Keys.D);

                        if (Key == Button.Up)
                            return keyboardStateCurrent.IsKeyDown(Keys.W);

                        if (Key == Button.Down)
                            return keyboardStateCurrent.IsKeyDown(Keys.S);

                        if (Key == Button.Jump)
                            return keyboardStateCurrent.IsKeyDown(Keys.Space);

                        if (Key == Button.Interact)
                            return keyboardStateCurrent.IsKeyDown(Keys.E);

                        if (Key == Button.Reload)
                            return keyboardStateCurrent.IsKeyDown(Keys.R);

                        break;
                    case PlayerIndex.Three:
                        break;
                    case PlayerIndex.Two:
                        break;
                    default:
                        break;
                }
            }
            #endregion

            #region ALTERNATEGAMEPAD
            if (IsAlternateGamePad)
            {
                switch (padNumber)
                {
                    case PlayerIndex.One:

                        //if (Key == Button.Left)
                            //return DIGamepad.

                        if (Key == Button.Right)
                            return keyboardStateCurrent.IsKeyDown(Keys.D);

                        if (Key == Button.Up)
                            return keyboardStateCurrent.IsKeyDown(Keys.W);

                        if (Key == Button.Down)
                            return keyboardStateCurrent.IsKeyDown(Keys.S);

                        if (Key == Button.Jump)
                            return keyboardStateCurrent.IsKeyDown(Keys.Space);

                        if (Key == Button.Interact)
                            return keyboardStateCurrent.IsKeyDown(Keys.E);

                        if (Key == Button.Reload)
                            return keyboardStateCurrent.IsKeyDown(Keys.R);

                        break;
                    case PlayerIndex.Two:
                        break;
                    case PlayerIndex.Three:
                        break;
                    case PlayerIndex.Four:
                        break;
                    default:
                        break;
                }
            }
            #endregion

            return false;
        }

        #endregion
        #region AnimationStates

        public bool[] AnimationState = new bool[20]; 

        //AnimationState[0] --> Player standing in a normal manner
        //AnimationState[1] --> Player crouching in a normal manner
        //AnimationState[2] --> Player running in a normal manner
        //AnimationState[3] --> Player running in a crawling manner
        //AnimationState[4] --> Player running in a exhausted manner
        //AnimationState[5] --> Player running in a speedy manner
        //AnimationState[6] --> Player walking in a normal manner
        //AnimationState[7] --> Player crawling in a normal manner
        //AnimationState[8] --> Player walking in a speedy manner
        //AnimationState[9] --> Player walking in a really slow manner

        //AnimationState[10] --> Player dying from being eaten by a flytrap
        //AnimationState[11] --> Player dying from being squashed
        //AnimationState[12] --> Player dying from drowning in lava
        //AnimationState[13] --> Player dying from drowning in mud
        //AnimationState[14] --> Player dying from drowning in water

        //AnimationState[15] --> Player jumping
        //AnimationState[16] --> Player swimming over water surface
        //AnimationState[17] --> Player swimming under water
        //AnimationState[18] --> Player firing heavy weapon standing
        //AnimationState[19] --> Player firing heavy weapon running
        //AnimationState[20] --> ???

        //AnimationState[21] --> ???
        //AnimationState[22] --> ???
        //AnimationState[23] --> ???
        //AnimationState[24] --> ???
        //AnimationState[25] --> ???
        //AnimationState[26] --> ???

        #endregion

        public Player(int controllerNumber) //VALUES FOR PLAYER ARE SET HERE
        { 
            collisionTexture = Game1.Instance.Content.Load<Texture2D>("Graphics/Tile-based Maps/Collision");
            CanMove = true;
            IsAnimationFrozen = false;
            ActiveCorpse = false;

            IsBulletFired = false;
            HasPlayerStabbed = false;

            AffectedByGravity = true;

            Direction.Y = 1;

            //CameraOffsetX = Randomizer.Next(0, 250);
            //CameraOffset = new Vector2(-400, Position.Y);
            //CameraOffsetY = 0;

            IsKeyboard = true;
            IsGamePad = false;
            IsAlternateGamePad = false;

            HasJumped = false;
            HasLanded = false;
            HasDied = false;
            IsRespawning = false;

            ShakeEffect = 0f;
            ShakeScreen = false;

            for (int i = 0; i < AnimationState.Length; i++)
            {
                AnimationState[i] = new bool();
            }

            FrameSizeX = 57;
            FrameSizeY = 41;

            OldDirection.X = 1;

            MaxVelocityX = 10f;
            MaxVelocityY = 10f;

            Radius = new Vector2(10, 10);
            Offset = new Vector2(0, 0);

            Angle = 0f;

            AnimationState[0] = true;

            BulletPower = 10f;

            AddRes = 1;

            Gravity = 1000f;
            VelocityY = MathHelper.Clamp(VelocityY, -400, 500); //Sets the maximum values for falling and jumping
        }

        public void ResetPlayer() //Reset the values of the player to their original values
        {
            Life = 100;

            CanMove = true;
            IsAnimationFrozen = false;
            AffectedByGravity = true;

            for (int i = 0; i < AnimationState.Length; i++)
            {
                if (i == 0)
                {
                    AnimationState[i] = true;
                }

                if (i > 0)
                {
                    AnimationState[i] = false;
                }
            }

            IsBulletFired = false;
            HasPlayerStabbed = false;
            IsExplosiveThrown = false;
            IsExplosiveActivated = false;

            CameraOffsetX = 0;
            CameraOffsetY = 0;

            HasJumped = false;
            HasLanded = false;
            HasDied = false;
            IsRespawning = true;

            ShakeScreen = false;
            ShakeEffect = 0f;

            GOTTAGOFAST = false;

            HealthBuff = false;
            ArmorBuff = false;
            WeaponBuff = false;
            JumpBuff = false;
            SpeedBuff = false;
            StabManiac = false;
        }

        public override void Update(GameTime gameTime)
        {
            GamePadState pad1 = Microsoft.Xna.Framework.Input.GamePad.GetState(padNumber);
            //ExtendedGamePadState pad1Alter = input.
            keyboardStateCurrent = Keyboard.GetState();
            mouseStateCurrent = Mouse.GetState();

            if (Game1.SameScreenMode || Game1.SplitScreenMode)
            {
                if (Keyboard.GetState().GetPressedKeys().Length > 0) //If the player is pressing any key...
                {

                }
            }
            //Angle = MathHelper.Clamp(Angle, MinAngle, MaxAngle); //Make sure the rotation of the player is limited, as we don't want the player spinning around at a demonic velocity, right?
            //ShakeEffect = MathHelper.Clamp(ShakeEffect, MinShake, MaxShake); //Make sure the shake effect is limited when the screen is shaking. Unless you're in for a racey-bumpy-roady simulation...

            switch (Game1.Instance.state)
            {
                case Game1.GameState.Ingame:
                    #region inthegame

                #region ANGLES
                #endregion

                #region CONTROLS
                
                Direction.X = 0; //SET THE HORISONTAL DIRECTION TO ZERO BEFORE CONTROL UPDATE TO MAKE SURE CHARACTER DOES STAY PUT WHEN CONTROL INPUT IS NOT ACTIVE
                VelocityX = 0;

                if (keyboardStateCurrent.IsKeyDown(Keys.A))
                {
                    Direction.X = -1;
                    OldDirection.X = Direction.X;

                    if (AnimationState[0] == true)
                    VelocityX -= 250f;

                    if (AnimationState[15] == true)
                    VelocityX -= 400f;
                }

                if (keyboardStateCurrent.IsKeyDown(Keys.D))
                {
                    Direction.X = 1;
                    OldDirection.X = Direction.X;

                    if (AnimationState[0] == true)
                        VelocityX += 250f;

                    if (AnimationState[15] == true)
                        VelocityX += 400f;
                }

                //VelocityY += Gravity;

                if (keyboardStateCurrent.IsKeyDown(Keys.W) && keyboardStateOld.IsKeyUp(Keys.W) && !HasJumped)
                {
                    HasJumped = true;
                    //if (HasJumped != true)
                    //{
                    //YVelAdd = 5;
                    //SetJump();
                    VelocityY -= 400f;
                    //}
                }

                if (keyboardStateCurrent.IsKeyDown(Keys.S))
                {
                    if (HasJumped != true)
                    {
                        //YVelAdd = 5;
                        //Direction.Y = 1;
                        //VelocityY += 1f;
                        //VelocityY -= 1f * (float)delta;
                    }
                }

                if (HasJumped == true && HasLanded == false)
                {
                    //DO SOMETHING
                }

                if (mouseStateCurrent.LeftButton == ButtonState.Pressed && mouseStateOld.LeftButton == ButtonState.Released)
                {
                    IsBulletFired = true;
                }

                if (IsBulletFired == true) //If the player is shooting...
                {
                    AddBullet();
                }

                #endregion

                #region RESOLUTION STUFF
                if (keyboardStateCurrent.IsKeyDown(Keys.G) && keyboardStateOld.IsKeyUp(Keys.G))
                {
                    Settings.SetFullScreen();
                }

                if (keyboardStateCurrent.IsKeyDown(Keys.F) && keyboardStateOld.IsKeyUp(Keys.F))
                {
                    if (AddRes + 1 != Settings.Resolutions.Length)
                        Settings.SetRes(Settings.Resolutions[0 + AddRes]);

                    else
                        AddRes = 0;
                        Settings.SetRes(Settings.Resolutions[0 + AddRes]);

                    AddRes++;
                }

                if (keyboardStateCurrent.IsKeyDown(Keys.Space) && keyboardStateOld.IsKeyUp(Keys.Space)) //If the player is pressing the space button
                {
                }

                if (Game1.Instance.DebugMode == true)
                {
                    if (keyboardStateCurrent.IsKeyDown(Keys.W))
                        FrameSizeX++;
                    if (keyboardStateCurrent.IsKeyDown(Keys.Q))
                        FrameSizeY++;
                }

                #endregion

                #region SPECIAL EFFECTS

                if (ShakeScreen == true) //If the screen CAN shake...
                {
                    //...then shake it! OOH BABEH
                    CameraOffsetX = Randomizer.Next(0, 30);
                    CameraOffsetY = Randomizer.Next(0, 30);
                    //Apply the offset shaking for effect and the shake effect for HOW effective it´ll shake.
                    CameraOffset = new Vector2(-400 + (CameraOffsetX * ShakeEffect), 0 + (CameraOffsetY * ShakeEffect));
                    //ShakeEffect -= 0.0002f;
                }

                if (ColorChange == true)
                {
                    
                }

                #endregion

                #region ANIMATION

                for (int i = 0; i < AnimationState.Length; i++)
                {

                }

                if (AnimationState[0])
                {
                    if (CurrentFrame.X >= 2 && CurrentFrame.Y != 4)
                    {
                        CurrentFrame.Y++;
                        CurrentFrame.X = 0;
                    }

                    if (CurrentFrame.Y == 4 && CurrentFrame.X >= 0)
                    {
                        CurrentFrame.X = 0;
                        CurrentFrame.Y = 0;
                    }
                }

                #endregion

                    #endregion
                    break;
                default:
                    break;
            }

            //Collisions - Used for checking if player collides with other objects. 
            //COLLISIONS WITH ENEMIES ARE NOW BEING KEPT IN THE ENEMY CLASSES, DUE TO PLAYER BEING A SINGLE OBJECT AND SIMPLYFYING THE CODE FOR LESS PROCESSIING USAGE
            //RESET FUNCTIONS AND ACTIONS OF COLLISIONS OCCUR IN THE OBSTACLE CLASS

            #region AnimationCalculation

            timer += (float)gameTime.ElapsedGameTime.TotalSeconds; //How long a frame takes

            if (timer >= frameLength)
            {
                timer = 0f; //start counter from zero
                CurrentFrame.X = (CurrentFrame.X + 1) % TotalFrames.X; //This part uses modulus to make the current spriteframe move further.
            }

            #endregion

            oldPad = pad1; //Make sure to register the previously pressed buttons...
            keyboardStateOld = keyboardStateCurrent; //...and keys.
            mouseStateOld = mouseStateCurrent; //Don't forget the mouse too!
            //oldDIGamepad = DIGamepad; //OR the alternative controllers!

            base.Update(gameTime); //Call the update from the inherited class which this update is based upon
            input.Update();

            CheckForUnWalkableTiles(); //Check for tiles the player can't collide with
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(GFX, Position, new Rectangle(FrameSize.X * CurrentFrame.X, FrameSize.Y * CurrentFrame.Y, FrameSize.X, FrameSize.Y), new Color(R, G, B, Alpha), Angle, new Vector2(FrameSize.X / 2, FrameSize.Y / 2), 1F, SpriteEffects.None, Layer); //Sets the draw function of the player object.
            DrawCollisionPoints(spriteBatch);
        }

        public void SetJump()
        {
            HasJumped = true; //The player is currently jumping
            HasLanded = false; //and is not colliding vertically with terrain/obstacles.
        }

        public void SetLand()
        {
            if (VelocityY <= -20f) //If the downwards velocity is too high...
            VelocityX = VelocityX - (VelocityX / 1.5f); //...then the player's running speed will be slowed down temporarily.

            //Direction.Y = 0;
            //VelocityY = 0;

            HasJumped = false; //The player isn't jumping anymore...
            HasLanded = true; //...he has landed instead.
        }

        public void SetDeathOrSpawn()
        {
            if (HasDied == false)
                HasDied = true;
            else
                HasDied = false;
        }

        public void SetStab()
        {
            if (HasPlayerStabbed == false)
                HasPlayerStabbed = true;
            else
                HasPlayerStabbed = false;
        }

        public void SetAndNormalizeBullet()
        {
            //Game1.Instance.MouseDistanceToLimit = (Game1.Instance.currentMousepos - this.Position).Length();
            //Game1.Instance.MouseDistanceToLimitX = (Game1.Instance.currentMousepos.X - this.Position.X).Length();
            //Game1.Instance.MouseDistanceToLimitVector = new Vector2(Game1.Instance.MouseposWorld.X - this.Position.X, Game1.Instance.MouseposWorld.Y - this.Position.Y);
            //Game1.Instance.MouseDistanceToLimitVector = (Game1.Instance.currentMousepos.X - this.Position.X).Length();
            Game1.Instance.MouseDistanceToLimitVector = Game1.Instance.MouseposWorld - this.Position;

            if (Game1.Instance.MouseDistanceToLimitVector != Vector2.Zero)
                Game1.Instance.MouseDistanceToLimitVector.Normalize();
        }

        public void AddBullet()
        {
            SetAndNormalizeBullet();

            Game1.Instance.Bullets.Add(new Bullet() //...then the player's weapon will fire ammunition.
                        {
                            GFX = Game1.Instance.BulletTextures[0], //Sets the texture of the fired ammunition
                            Angle = (float)Math.Atan2(Game1.Instance.MouseDistanceToLimitVector.Y, Game1.Instance.MouseDistanceToLimitVector.X), //Sets which angle of the bullet will be fired from. In this case, it's the angle of the player.
                            VelocityX = 10 + this.VelocityX, //If the player is moving, then the bullet will gain a faster speed. Otherwise, the value of the speed of the bullet is set to 50.
                            VelocityY = 10 + this.VelocityY,
                            Bulletis = this.BulletPower, //Sets the gunpower of the player's weapon.
                            Direction = Game1.Instance.MouseDistanceToLimitVector, //Sets the direction of the bullet.
                            //Direction = new Vector2((float)Math.Sin(Angle), (float)-Math.Cos(Angle))
                            Position = this.Position, //From where the ammunition will be fired
                            AffectedByGravity = false,
                        });
            //ShotPower = 0;
            IsBulletFired = false; //When the ammo/bullet is fired, the code will set this activation bool to a false value. Which means the player isn't firing at all.
        }

        public void CheckForUnWalkableTiles()
        {
            TileMap map = Game1.Instance.Map1; //Shortcut to lvl1.

            Vector2 middlePoint = Position + Offset; //Middle of the collision.

            for (int i = 0; i < CollisionPoints.Length; i++)
            {
                Vector2 positionInPixel = middlePoint + CollisionPoints[i];
                Point cellPosition = Settings.Tiles.ConvertPositionToCell(positionInPixel);

                cellPosition.X = (int)MathHelper.Clamp(cellPosition.X, 0, Settings.Tiles.tileMapWidth-1);
                cellPosition.Y = (int)MathHelper.Clamp(cellPosition.Y, 0, Settings.Tiles.tileMapHeight-1);

                if (map.Collision.GetCellIndex(cellPosition) == 1)
                {
                    Rectangle posRect = Settings.Tiles.CreateRectForCell(cellPosition); //Convert tilemap position into rectangle position (pixels).

                    Vector2[] moveCases = new Vector2[2]; //Save impossible move position.
                    bool[] possibleMoves = new bool[2];
                    possibleMoves[0] = true;
                    possibleMoves[1] = true;

                    if (CollisionPoints[i].X > 0) //Right side from middle.
                    {
                        if (cellPosition.X - 1 > 0 && map.Collision.GetCellIndex(new Point(cellPosition.X - 1, cellPosition.Y)) == 1)
                            possibleMoves[0] = false;

                        moveCases[0] = new Vector2(posRect.X - CollisionPoints[i].X, middlePoint.Y);
                    }
                    else //Left side from middle.
                    {
                        if (cellPosition.X + 1 < Settings.Tiles.tileMapWidth && map.Collision.GetCellIndex(new Point(cellPosition.X + 1, cellPosition.Y)) == 1)
                            possibleMoves[0] = false;

                        moveCases[0] = new Vector2(posRect.X + posRect.Width - CollisionPoints[i].X, middlePoint.Y);
                    }

                    if (CollisionPoints[i].Y > 0) //Below middle of the sprite.
                    {
                        if (cellPosition.Y - 1 > 0 && map.Collision.GetCellIndex(new Point(cellPosition.X, cellPosition.Y - 1)) == 1)
                            possibleMoves[1] = false;

                        moveCases[1] = new Vector2(middlePoint.X, posRect.Y - CollisionPoints[i].Y);
                    }

                    else //Above middle of the sprite.
                    {
                        if (cellPosition.Y + 1 < Settings.Tiles.tileMapHeight && map.Collision.GetCellIndex(new Point(cellPosition.X, cellPosition.Y + 1)) == 1)
                            possibleMoves[1] = false;

                        moveCases[1] = new Vector2(middlePoint.X, posRect.Y + posRect.Height - CollisionPoints[i].Y);
                    }

                    Vector2 newPosition = middlePoint;

                    if ((moveCases[0] - middlePoint).Length() > (moveCases[1] - middlePoint).Length() && possibleMoves[1] || !possibleMoves[0])
                    {
                        newPosition = moveCases[1];

                        if (middlePoint.Y < newPosition.Y)
                            VelocityY += 10;
                        else
                        {
                            HasJumped = false;
                            VelocityY = 0;
                        }
                    }

                    else
                        newPosition = moveCases[0];

                    middlePoint = newPosition;

                }
            }

            Position = middlePoint - Offset; //Remove offset info.
        }

        public void DrawCollisionPoints(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < CollisionPoints.Length; i++) //For every collision point of the player
            {
                //Draw a visual representation of each collisionpoint
                spriteBatch.Draw(collisionTexture, Position + Offset + CollisionPoints[i], null, Color.Red, 0, new Vector2(collisionTexture.Width / 2, collisionTexture.Height / 2), 1, SpriteEffects.None, 1);
            }
        }
    }
}
