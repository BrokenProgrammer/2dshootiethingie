﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    class MultiplayerManager
    {
        public enum MultiPlayerGameMode 
        {
            Capture_The_Blafgh, //Capture a "blafgh" from the enemy team and bring it back to base for points.
            HellFire, //Jump 'n 'shoot away from a rising lava crisis for survival - Last man standing is winner!
            Lands_Of_Blasts, //Teamwork killing against enemy team on paralell layers(Rayman 2 Alpha as inspiration) - First team to destroy the others buildnings wins!
            Down_Under, //Team/Normal deathmatch in a destructible level. The level progresses further down and down as more of the level is destroyed - Last man/team to be alive down under wins!
            Death_By_Killing, //Normal team/deathmatch
        }

        public enum MultiPlayerMap
        {
            Rising_Scratchboard, //HELLFIRE MODE - STONE/GRASS
            Temple_Of_Creetuclaij, //DOWN UNDER - TEMPLE/GRASS/CAVE/STONE/METAL
            Two_Horizons, //LAND OF BLASTS -GRASS/FOREST
            Trezmurrs, //CAPTURE THE BLAFGH - DESERT
            Skooegzmiljue, //DEATH BY KILLING - FOREST
            Dry_Warts, //LAND OF BLASTS - DESERT
        }

        public bool AllowPowerups = true; //If powerups should be allowed or not
        public bool AllowDestrucibleTiles = true; //If destructible tiles should be allowed or not

        public float SpeedMultiplier = 1; //Multiplier of player movement in match
        public float DamageMultiplier = 1; //Multiplier of total player damage in match

        public float TimeLimit = 0; //The time limit of the current match. IF SET TO ZERO - NO TIME LIMIT
    
        public MultiPlayerGameMode MPModeState;
        public MultiPlayerMap MPMapState;


        //FOR NETWORK RATINGS
        float SERVER_RATING; //CLAMPED BETWEEN 0 AND 5
        bool ISSERVERBAD;
        bool ISSERVERGOOD;
    }
}
