﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    public class Bullet : MovObj
    {
        public float[] BulletForce;
        public float Bulletis;

        public Bullet()
        {
            BulletForce = new float[21];
            for (int i = 0; i < BulletForce.Length; i++)
            {
                BulletForce[i] = new float();
                BulletForce[i] = (1f + (float)i);
            }
        }

        public override void Update(GameTime gameTime)
        {
            //for (int i = 0; i < Game1.Instance.Bullets.Count; i++)
            //{
            //    if (Bullets[i].Power < 0) //If the power of the bullet is below zero...
            //    Bullets.RemoveAt(i); //...then the bullet will be deleted.     
            //}

            VelocityX = 10;
            VelocityY = 10;
            Bulletis -= 0.05F;

            base.Update(gameTime);
        }
    }
}
