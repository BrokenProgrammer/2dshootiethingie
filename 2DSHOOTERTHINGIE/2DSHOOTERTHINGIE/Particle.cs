﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _2DSHOOTERTHINGIE
{
    class Particle
    {
        public Texture2D Texture; //The graphics of the particle
        public Vector2 Position; //Where the particle is on the screen
        public Vector2 Velocity; //The primary velocity of the particle
        public float Angle; //The angle of the particle
        public float AngularVelocity; //The secondary velocity of the particle, changing the speed of the rotating angle
        public Color Colour; //The color of the particle
        public float Size; //The size of the particle
        public int TimeToLive; //Length of the particle's lifetime
        public int ScreenBottomEdge; //Used for detecting the bottom of the screen
        public float Layer;

        int startTimeToLive;
        float gravity; //The gravity affecting the particle

        public Particle(Texture2D texture, Vector2 position, Vector2 velocity, float angle, float angularVelocity, Color colour, float size, int timeToLive)
        {
            //These are for defining the properties of the particle
            Texture = texture;
            Position = position;
            Velocity = velocity;
            Angle = angle;
            AngularVelocity = angularVelocity;
            Colour = colour;
            Size = size;
            TimeToLive = timeToLive;
            startTimeToLive = timeToLive;
            gravity = 500;
            //ScreenBottomEdge = GameSettings.ScreenWidth - Texture.Height;
            Layer = 1f;
        }

        public void Update(GameTime gameTime)
        {
            //if the particle starts to spawn, then begin to randomize the colour of the spawning particles.
            if (startTimeToLive > 0)
            {
                float percent = (float)TimeToLive / startTimeToLive;
                Colour.A = (byte)(255 * percent);
            }

            //If the particle collides with the bottom of the screen, then make it bounce/smash/whatever kind of effect you want to use
            if (Position.Y > ScreenBottomEdge)
            {
                Velocity.Y = Velocity.Y - (Velocity.Y * 2);
                Velocity.Y *= 0.7f;
                Velocity.X *= 0.7f;
            }

            //Kill the particle when it's life cycle ends
            TimeToLive--;

            //Calculate the gravity in realtime and make sure it is limited for the SAFETY OF OUR PEOPLE
            Velocity.Y += gravity * (float)gameTime.ElapsedGameTime.TotalSeconds;
            Velocity.Y = MathHelper.Clamp(Velocity.Y, -1000, 500);

            //Make sure the speed of the particle is dependent of realtime calculation and not computer speed
            //and make the particle's angular rotation dependent on the velocity value, to make it spin
            Position += Velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;
            Angle += AngularVelocity;
            //Angle = Angle + AngularVelocity;
        }

        public void Draw(SpriteBatch spriteBatch) //Used for drawing the particle
        {
            Rectangle sourceRectangle = new Rectangle(0, 0, Texture.Width, Texture.Height); //Draw the rectangle of the particle
            Vector2 origin = new Vector2(Texture.Width / 2, Texture.Height / 2); //Detect the particle origin

            spriteBatch.Draw(Texture, Position, sourceRectangle, Colour, Angle, origin, Size, SpriteEffects.None, Layer); //Draw the particle
        }
    }
}
